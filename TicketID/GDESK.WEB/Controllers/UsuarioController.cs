﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using GDESK.CORE.Enum;
using GDESK.CORE.Filter;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.UserControl;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.WEB.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly IAnexoService _anexoService;
        private readonly IUsuarioService _usuarioService;
        private readonly IEmpresaService _empresaService;

        public string GuidId
        {
            get { return Guid.NewGuid().ToString(); }
        }

        public UsuarioController()
        {
            _anexoService = new AnexoService();
            _usuarioService = new UsuarioService();
            _empresaService = new EmpresaService();
        }

        public ActionResult Autenticar()
        {
            var session = Sessions.UsuarioLogado;

            if (session == null)
            {
                Sessions.Abas.Clear();
                return View();
            }
            

            var model = new ViewModelLogin { Login = session.Login, Senha = session.Senha };
            _usuarioService.Autenticar(model);

            return RedirectToAction("Principal", "Chamado");
        }

        [HttpPost]
        public ActionResult Autenticar(ViewModelLogin model)
        {
            var url = Url.Action("Principal", "Chamado");

            if (!string.IsNullOrEmpty(model.UrlDefault))
                url = model.UrlDefault;

            if (_usuarioService.Autenticar(model)) return Json(url);

            return new HttpStatusCodeResult(HttpStatusCode.Conflict, "Fault");
        }


        [HttpPost]
        public ActionResult Cadastrar(UsuarioViewModel model)
        {
           
             var codAnexo = 1; //~/Profiles/padrao.jpg padrão

            if (Sessions.Anexos.Any())
                codAnexo = _anexoService.Adicionar(Sessions.Anexos.First());

            Sessions.Anexos.Clear();

            model.CodigoAnexo = codAnexo;
            _usuarioService.AdicionarUsuario(model);

            var login = new ViewModelLogin { Login = model.Login, Senha = model.Senha };

            return Json(_usuarioService.Autenticar(login) ? Url.Action("Principal", "Chamado") : "Fault");
        }

        [Authorize]
        public ActionResult Alterar()
        {
            var model = Suporte.UsuarioLogado;
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Alterar(UsuarioViewModel model)
        {
    
            if (model == null) throw new ArgumentNullException("model");

            if (Sessions.Anexos.Any())
                model.CodigoAnexo = _anexoService.Adicionar(Sessions.Anexos.First());

            Sessions.Anexos.Clear();

            _usuarioService.AlterarUsuario(model);

            Sessions.UsuarioLogado = _usuarioService.ObterUsuario(model.CodigoUsuario);

            return View(Suporte.UsuarioLogado);
        }

        [TrocarStatusChamadoFilter]
        [Authorize]
        public ActionResult Deslogar()
        {
            _usuarioService.Logout();

            return RedirectToAction("Autenticar");
        }

       
        [HttpPost]
        public ActionResult UploadProfile()
        {
            var fileName = string.Empty;
            var pathFile = string.Empty;
            const string mensagem = " Arquivo recebido com êxito !";


            foreach (var anexo in from string file in Request.Files select Request.Files[file])
            {
                var fileToSave = GuidId + Path.GetExtension(anexo.FileName);
                pathFile = Path.Combine("~/Profiles", fileToSave);
                fileName = anexo.FileName;


                _anexoService
                    .FileName(Server.MapPath(pathFile))
                    .Arquivo(anexo);

                if (Sessions.Anexos.Any())
                    _anexoService.Deletar(Server.MapPath(Sessions.Anexos.First().CaminhoArquivo));

                Sessions.Anexos = new List<AnexoViewModel> { new AnexoViewModel { CaminhoArquivo = pathFile, NomeArquivo = anexo.FileName } };

                _anexoService.Anexar(DestinoAnexoEnum.Abrir);


            }

            return Json(new { FileName = fileName, Mensagem = mensagem, PathName = pathFile });
        }


        #region JSONRESULT

       
        public JsonResult VerificaEmpresa(string cnpj)
        {
            var empresa = _empresaService.VerificaEmpresa(cnpj);


            return Json(new { Existe = empresa != null, NomeEmpresa = empresa != null ? empresa.Nome : "Invalido", CodigoEmpresa = empresa != null ? empresa.CodigoEmpresa : 0 }, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult VerificaUsuario(string usuarioNome)
        {
            var usuario = _usuarioService.VerificaUsuario(usuarioNome);

            return Json(new { Existe = usuario != null, NomeUsuario = usuario != null ? usuario.Nome : "Invalido", CodigoUsuario = usuario != null ? usuario.CodigoUsuario : 0 }, JsonRequestBehavior.AllowGet);
        }


        #endregion

    }
}


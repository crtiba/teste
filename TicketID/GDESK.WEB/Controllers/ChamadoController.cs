﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using GDESK.CORE.Enum;
using GDESK.CORE.Filter;
using GDESK.CORE.Query;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.UserControl;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;
using Microsoft.Ajax.Utilities;

namespace GDESK.WEB.Controllers
{
    public class ChamadoController : Controller
    {
        //
        // GET: /Chamado/

        private readonly ConsultaViewModel _consultaViewModel;
        private readonly IChamadoService _chamadoService;
        private readonly IAnexoService _anexoService;
        private readonly ResponderChamadoViewModel _responderChamadoViewModel;
        private readonly IListaDeChamadosQuery _listaDeChamadosQuery;
        private readonly ChamadoViewModel _chamadoViewModel;
        private readonly IHistoricoChamadoService _historicoChamadoService;
        private readonly IHorario _horario;
        private HistoricoChamadoViewModel _historicoChamadoViewModel;
        private readonly IUsuarioService _usuarioService;
        private readonly IEmailService _emailService;

        private ISmsService _smsService;


        public ChamadoController()
        {
            _chamadoService = new ChamadoService();
            _consultaViewModel = new ConsultaViewModel();
            _anexoService = new AnexoService();
            _responderChamadoViewModel = new ResponderChamadoViewModel();
            _listaDeChamadosQuery = new ListaDeChamadosQuery();
            _chamadoViewModel = new ChamadoViewModel();
            _historicoChamadoService = new HistoricoChamadoService();
            _horario = new Horario();
            _usuarioService = new UsuarioService();
            _emailService = new EmailService();


        }
        [Authorize]
        public ActionResult Principal()
        {

            return View();
        }
        [Authorize]
        public ActionResult Abrir()
        {
            _chamadoViewModel.Usuario = Suporte.UsuarioLogado;
            return View(_chamadoViewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Abrir(ChamadoViewModel model)
        {

            model.CodigoAnalista = (int)AnalistaEnum.Padrao;
            model.CodigoStatus = (int)StatusChamadoEnum.Novo;
            model.Usuario = _usuarioService.ObterUsuario(model.CodigoSolicitante);

            var codChamado = _chamadoService.AbrirChamado(model);


            _smsService = new SmsService(codChamado);
            _smsService.Adicionar(model.Usuario.Celular);
            _smsService.Enviar();


            _historicoChamadoViewModel = new HistoricoChamadoViewModel
            {
                CodigoChamado = codChamado,
                CodigoAnalista = (int)AnalistaEnum.Padrao,
                CodigoStatus = (int)StatusChamadoEnum.Novo,
                De = _horario.Agora()
            };

            _historicoChamadoService.AdicionarHistorico(_historicoChamadoViewModel);

            if (Sessions.Anexos.Count == 0) return Json(Url.Action("Responder", "Chamado", new { id = codChamado }));

            Sessions.Anexos.ForEach(x =>
            {
                x.CodigoChamado = codChamado;
                _anexoService.Adicionar(x);
            });

            Sessions.Anexos.Clear();


            return Json(Url.Action("Responder", "Chamado", new { id = codChamado }));
        }

       
        [Authorize]
        [TrocarStatusChamadoFilter]
        public ActionResult Consultar()
        {
            var lst = _listaDeChamadosQuery.Paramter(new FiltroChamdoViewModel { CodigoEmpresa = Suporte.UsuarioLogado.Analista ? 0 : Suporte.UsuarioLogado.CodigoEmpresa }).Execute().ToList();

            _consultaViewModel.HeaderViewModel = new ChamadoHeaderViewModel
            {
                Alta = lst.Count(x => x.Prioridade.Equals("Alta")),
                Baixa = lst.Count(x => x.Prioridade.Equals("Baixa")),
                Media = lst.Count(x => x.Prioridade.Equals("Media"))
            };
            return View(_consultaViewModel);
        }
        [Authorize]
        [RestrigeChamadoFilter]
        public ActionResult Responder(int id)
        {
            if (!Suporte.Abas.Any(x=>x.Equals(id)))
            Suporte.Abas.Add(id);

            _responderChamadoViewModel.DetalhesChamadoViewModel = _chamadoService.ObterDetalhesChamado(id);
            _responderChamadoViewModel.Chamado = _chamadoService.ObterChamado(id);
            _responderChamadoViewModel.Usuario = Suporte.UsuarioLogado;

            var conversas = _chamadoService.ObterConversa(id).OrderByDescending(x => x.DataResposta).ToList();

            if (!Suporte.UsuarioLogado.Analista)
                conversas = conversas.Where(x => x.Privado != true).ToList();

            _responderChamadoViewModel.Conversas = conversas;
            _responderChamadoViewModel.Anexos = _chamadoService.ObterAnexos(id);


            return View(_responderChamadoViewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Responder(RespostaViewModel model)
        {
            model.CodigoResposta = _chamadoService.AdicionarResposta(model);


            Sessions.Anexos.ForEach(x =>
            {
                x.CodigoResposta = model.CodigoResposta;
                _anexoService.Adicionar(x);
            });

            model.Anexos = _anexoService.ObterAnexos(model.CodigoResposta);

            Sessions.Anexos.Clear();

            var solicitante = _usuarioService.ObterUsuarioPorCodigoChamado(model.CodigoChamado);
            if (solicitante != null && !model.Privado)
                _emailService.AlterarStatusEmail(string.Format("Analista Respondeu: {0}", model.Mensagem), "Respondido", model.CodigoChamado, solicitante.Email);

            return Json(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UltimasRespostas(int codigoChamado, int codigoUltimaResposta)
        {
            var lst = _chamadoService.UltimasRespostas(codigoChamado, codigoUltimaResposta);

            if (!Suporte.UsuarioLogado.Analista)
                lst = lst.Where(x => x.Privado == false).ToList();

            return Json(lst);
        }

    }
}

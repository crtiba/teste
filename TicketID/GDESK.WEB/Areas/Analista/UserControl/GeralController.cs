﻿using System.Web.Mvc;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Page;

// ReSharper disable once CheckNamespace
namespace GDESK.WEB.UserControl.Controlller
{
    public partial class GeralController
    {
 
        [HttpPost]
        public void CadastrarEmpresa(EmpresaViewModel model)
        {
            _empresaService.AdicionarEmpresa(model);
        }

    }
}

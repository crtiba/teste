﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using GDESK.CORE.Enum;
using GDESK.CORE.Filter;
using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Query;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;
using GDESK.CORE.UserControl;

namespace GDESK.WEB.UserControl.Controlller
{
    public partial class GeralController : Controller
    {
        private readonly IAnexoService _anexoService;
        private readonly IMappingAnexo _mappingAnexo;
        private readonly IChamadoService _chamadoService;
        private readonly IListaDeChamadosQuery _listaDeChamadosQuery;
        private readonly IUsuarioService _usuarioService;
        private readonly IEmailService _emailService;
        private readonly IStatusService _statusService;
        private readonly IEmpresaRepository _empresaRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        public static IList<int> Chamado = new List<int>();
        private readonly IEmpresaService _empresaService;
        private readonly IMappingUsuario _mappingUsuario;

        public string GuidId
        {
            get { return Guid.NewGuid().ToString(); }
        }

        public GeralController()
        {
            _anexoService = new AnexoService();
            _chamadoService = new ChamadoService();
            _listaDeChamadosQuery = new ListaDeChamadosQuery();
            _emailService = new EmailService();
            _statusService = new StatusService();
            _usuarioService = new UsuarioService();
            _empresaRepository = new EmpresaRepository();
            _usuarioRepository = new UsuarioRepository();
            _empresaService = new EmpresaService();
            _mappingUsuario = new MappingUsuario();
            _mappingAnexo = new MappingAnexo();

        }

        #region GRIDCHAMADOS
        [Authorize]
        public ActionResult Lista()
        {
            Sessions.ListaChamado = _listaDeChamadosQuery.Paramter(new FiltroChamdoViewModel { CodigoEmpresa = Suporte.UsuarioLogado.Analista ? 0 : Suporte.UsuarioLogado.CodigoEmpresa }).Execute().OrderByDescending(x=>x.DataAbertura).ToList();

            return PartialView("~/UserControl/GridView/_PartialGridChamadoData.cshtml", Sessions.ListaChamado);
        }

        [Authorize]
        [HttpPost]
        public PartialViewResult More(int pageIndex, int pageSize)
        {
            return PartialView("~/UserControl/GridView/_PartialGridChamadoData.cshtml", Sessions.ListaChamado);
        }
        [Authorize]
        [HttpPost]
        public PartialViewResult Filtro(FiltroChamdoViewModel model)
        {


            Sessions.ListaChamado.Clear();
            model.Fechados = true;

            if (!Suporte.UsuarioLogado.Analista)
                model.CodigoEmpresa = Suporte.UsuarioLogado.CodigoEmpresa;

            Sessions.ListaChamado = _listaDeChamadosQuery.Paramter(model).Execute().OrderByDescending(x => x.DataAbertura).ToList();

            return PartialView("~/UserControl/GridView/_PartialGridChamadoData.cshtml", Sessions.ListaChamado);
        }
        [Authorize]
        public PartialViewResult SortingList(GridChamadoColumns column, string ordem)
        {
            switch (column)
            {
                case GridChamadoColumns.Codigo:
                    Sessions.ListaChamado = ordem == "DESC" ?
                        new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.CodigoChamado)) :
                        new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.CodigoChamado));
                    break;
                case GridChamadoColumns.Cliente:
                    Sessions.ListaChamado = ordem == "DESC" ?
                        new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.NomeCliente)) :
                        new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.NomeCliente));
                    break;
                case GridChamadoColumns.Empresa:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.NomeEmpresa))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.NomeEmpresa));
                    break;
                case GridChamadoColumns.Assunto:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.Assunto))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.Assunto));
                    break;
                case GridChamadoColumns.Status:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.Status))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.Status));
                    break;
                case GridChamadoColumns.DataAbertura:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.DataAbertura))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.DataAbertura));
                    break;
                case GridChamadoColumns.Analista:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.NomeAnalista))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.NomeAnalista));
                    break;
                case GridChamadoColumns.Tempo:
                    Sessions.ListaChamado = ordem == "DESC"
                        ? new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderByDescending(x => x.PrazoAtendimento))
                        : new List<GridChamadoViewModel>(Sessions.ListaChamado.OrderBy(x => x.PrazoAtendimento));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("column");
            }


            return PartialView("~/UserControl/GridView/_PartialGridChamadoData.cshtml", Sessions.ListaChamado);
        }

        #endregion

        #region UPLOAD
        [Authorize]
        [HttpPost]
        public ActionResult UploadFiles()
        {
            var fileName = string.Empty;
            var pathFile = string.Empty;
            const string mensagem = " Arquivo recebido com êxito !";


            foreach (var anexo in from string file in Request.Files select Request.Files[file])
            {
                var fileToSave = GuidId + Path.GetExtension(anexo.FileName);
                pathFile = Path.Combine("~/Anexos", fileToSave);
                fileName = anexo.FileName;

                Sessions.Anexos.Add(new AnexoViewModel { CaminhoArquivo = pathFile, NomeArquivo = anexo.FileName });

                _anexoService
                .FileName(Server.MapPath(pathFile))
                .Arquivo(anexo)
                .Anexar(DestinoAnexoEnum.Abrir);

            }

            return Json(new { FileName = fileName, Mensagem = mensagem, PathName = pathFile });
        }
        #endregion

        [Authorize]
        [HttpPost]
        public ActionResult ConfigurarNavBar(NavBarStatusViewModel model)
        {
            switch (_chamadoService.ObterStatus(model.CodigoChamado))
            {
                case StatusChamadoEnum.Novo:
                    model.Novo = true;
                    break;
                case StatusChamadoEnum.Atendimento:
                    model.Atendimento = true;
                    break;
                case StatusChamadoEnum.Reaberto:
                    model.Reaberto = true;
                    break;
                case StatusChamadoEnum.Aguardando:
                    model.Aguardando = true;
                    break;
                case StatusChamadoEnum.Finalizado:
                    model.Finalizado = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return Json(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult TrocarStatus(int codigoChamado, StatusChamadoEnum stausEnum)
        {
            var usuarioLogado = Suporte.UsuarioLogado;
            var solicitante = _usuarioService.ObterUsuarioPorCodigoChamado(codigoChamado);

            _chamadoService.TrocarStatus(stausEnum, codigoChamado, usuarioLogado.Analista ? usuarioLogado.CodigoUsuario : (int?)null);

            var model = new NavBarStatusViewModel { CodigoChamado = codigoChamado, Analista = usuarioLogado.Analista ? usuarioLogado.Nome : null };

            switch (stausEnum)
            {
                case StatusChamadoEnum.Atendimento:
                    model.Atendimento = true;
                    break;
                case StatusChamadoEnum.Reaberto:
                    model.Reaberto = true;
                    break;
                case StatusChamadoEnum.Aguardando:
                    model.Aguardando = true;
                    break;
                case StatusChamadoEnum.Finalizado:
                    model.Finalizado = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("stausEnum");
            }

            if (solicitante != null)
                _emailService.AlterarStatusEmail("O Seu chamado foi alterado, para mais informacoes click no botão abaixo", _statusService.ObterNomeStatus(stausEnum), codigoChamado, solicitante.Email);

            return Json(model);


        }


        [HttpPost]
        [Authorize]
        public ActionResult TrocarAnalista(int codigoChamado, int codigoAnalista)
        {
            var analista = _chamadoService.TrocarAnalista(codigoChamado, codigoAnalista);
            return Json(new { Analista = analista, Pagina = RenderRazorViewToString("~/UserControl/Responder/_PartialTransferirAnalista.cshtml", Suporte.TrocarAnalistaLista()) });
        }

        [HttpGet]
        public int NovoFeed()
        {
            Chamado.Add(5);
            return Chamado.FirstOrDefault();
        }
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        #region JSON

        [Authorize]
        public JsonResult Empresas()
        {
            return
                Json(_empresaRepository.ObterTodas()
                    .Select(x => new { value = x.pk_int_empresa, text = x.vch_nome.ToUpper() }).OrderBy(order => order.text), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Solicitantes(int codigoEmpresa)
        {
            return
                Json(_usuarioRepository.ObterUsuarios(x => x.Empresa_pk_int_empresa == codigoEmpresa).Select(x => new { value = x.pk_int_usuario, text = x.vch_nome.ToUpper() }).OrderBy(order => order.text), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Solicitante(int codigoUsuario)
        {
            var get = _usuarioRepository.ObterUsuario(x => x.pk_int_usuario == codigoUsuario);
            if (get == null) return null;
            get.Anexo = _mappingAnexo.MappingAnexoViewModelToAnexto(_anexoService.Anexo(get.Anexo_pk_int_anexo));
            var usuario = _mappingUsuario.MappingUsuaroToViewModelUsuario(get);

            
            
            return Json(usuario , JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        [TrocarStatusChamadoFilter]
        public void FecharAba(int codigoChamado)
        {
            Suporte.Abas.Remove(codigoChamado);
        }

        [HttpPost]
        public void AdicionarAba(int codigoChamado)
        {
            if (Suporte.Abas.All(x => x != codigoChamado))
            Suporte.Abas.Add(codigoChamado);
        }

    }
}

﻿using GDESK.CORE.UserControl;
using Microsoft.AspNet.SignalR;

namespace GDESK.WEB.Hubs
{
    public class NewFeed:Hub
    {
        public void EnviarMensagem(string solicitante, string avatar, string assunto, string mensagem, string url)
        {
            Clients.All.ReceberMensagem(solicitante, avatar, assunto, mensagem, url);
        }

        public void Notificar(string msg, bool analista, int codigoChamado)
        {
           
            Clients.All.Notificar(msg, analista, codigoChamado);
        }
    }
}
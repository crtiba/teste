﻿namespace GDESK.CORE.Enum
{
    public enum StatusChamadoEnum
    {
      
        Novo = 1,
        Atendimento,
        Reaberto,
        Aguardando,
        Finalizado
    }
}

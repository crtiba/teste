﻿using System.ComponentModel;

namespace GDESK.CORE.Enum
{
    public enum TipoHora
    {
        [Description("Normal")]
        Normal = 1,
        [Description("Extra")]
        Extra
    }
}

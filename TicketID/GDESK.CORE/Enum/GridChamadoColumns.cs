﻿namespace GDESK.CORE.Enum
{
    public enum GridChamadoColumns
    {
        Codigo,
        Cliente,
        Empresa,
        Assunto,
        Status,
        DataAbertura,
        Analista,
        Tempo
    }

    public enum OrderPor
    {
        Desc,
        Asc
    }
}

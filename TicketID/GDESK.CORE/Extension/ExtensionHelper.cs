﻿using GDESK.CORE.Enum;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Extension
{
    public static class ExtensionHelper
    {
        public static StringBuilder Builder { get; set; }

        static ExtensionHelper() { Builder = new StringBuilder(); }

        public static MvcHtmlString GridView(this HtmlHelper htmlHelper, string controllerName, string actionName, string btnAction)
        {


            var moreScript = string.Format(
                            @"$.ajax({{
                            url: '/{0}/More',
                            type:'POST',
                            content: 'application/json; charset=utf-8',
                            dataType: 'html',
                            data:{{ pageIndex: parseInt($('input[name=qtdGrid]').val()), pageSize: 10 }},
                            success:function(data) {{
                                $('body').html(data);
                            }}
                            }});", controllerName);



            Builder.AppendLine("<script>");

            Builder.AppendFormat("$('#btnMore').click(function(){{{0}}});", moreScript);


            Builder.AppendLine("</script>");

            return new MvcHtmlString(Builder.ToString());

        }

        public static int Valor(this StatusChamadoEnum e)
        {
            return (int)e;
        }

        public static int Valor(this GridChamadoColumns e)
        {
            return (int)e;
        }

        public static IHtmlString SelectList(this HtmlHelper html, Dictionary<int, string> items, string selectName, string labelName,
            string emptySelected = "Selecione", int selectedValue = 0)
        {
            Builder = new StringBuilder();
            Builder.AppendFormat("<label for='{0}' class='col-sm-2 control-label'>{1}</label>", selectName, labelName);
            Builder.AppendFormat("<div class='col-sm-10'><select id='{0}' class='form-control input-sm'>", selectName);
            items.ToList().ForEach(x => Builder.AppendFormat("<option value='{0}'>{1}</option>", x.Key, x.Value));

            Builder.AppendLine("</select></div>");

            return new HtmlString(Builder.ToString());
        }

        public static IHtmlString ComboBox(this HtmlHelper html, Dictionary<int, string> items, string dropName, string emptySelected = "Selecione", int selectedValue = 0)
        {

            Builder = new StringBuilder();

            Builder.Append("<div class='btn-group'>");
            Builder.AppendFormat("<button type='button' id='btn{0}' class='btn btn-primary' data-value-selected='{1}'>{2}</button>", dropName, selectedValue, emptySelected);
            Builder.Append("<button type='button' class='btn btn-info dropdown-toggle' data-toggle='dropdown' data-original-title=''>");
            Builder.Append("<span class='caret'></span></button>");
            Builder.AppendFormat("<ul class='dropdown-menu' id='{0}' role='menu'>", dropName);
            Builder.AppendFormat("<li><a href='javascript:void(0);' data-value='{0}'>{1}</a></li>", selectedValue, emptySelected);
            items.ToList().ForEach(x => Builder.AppendFormat("<li><a href='javascript:void(0);' data-value='{0}'>{1}</a></li>", x.Key, x.Value));
            Builder.Append("</ul></div>");

            Builder.Append("<script type='text/javascript'>");
            Builder.AppendFormat("$('#{0} li a').click(function () {{", dropName);
            Builder.AppendFormat("$('#btn{0}').text($(this).text());", dropName);
            Builder.AppendFormat("$('#btn{0}').data('value-selected', $(this).data('value')); }});", dropName);
            Builder.Append("</script>");

            return new HtmlString(Builder.ToString());

        }

        public static IHtmlString IdGraficoPizza(this HtmlHelper html, string nomeGrafico,
                                                                                      string tituloGrafico,
                                                                                      Dictionary<object, object> colunas,
                                                                                      Dictionary<string, int> linhas,
                                                                                      int width = 500,
                                                                                      int height = 500)
        {
            var strGrafico = new StringBuilder();

            //Cria div do grafico
            strGrafico.AppendFormat("<div id='{0}'></div>", nomeGrafico);
            //Começa scripty
            strGrafico.AppendLine("<script type='text/javascript'>");

            //Começa escript grafico
            strGrafico.AppendLine("google.load('visualization', '1.0', { 'packages': ['corechart'] });");
            strGrafico.AppendLine("google.setOnLoadCallback(drawChart);");


            //FUNCAO DRAWCHART
            strGrafico.AppendLine("function drawChart() {");
            strGrafico.AppendLine("var data = new google.visualization.DataTable();");

            colunas.ToList().ForEach(x => strGrafico.AppendFormat("data.addColumn('{0}', '{1}');", x.Key, x.Value));

            strGrafico.AppendLine("data.addColumn({type: 'string', role: 'tooltip'});");


            //FIM FUNCAO DRAWCHART

            ////Adiciona as linhas do gráfico
            linhas.ToList().ForEach(x => strGrafico.AppendFormat("data.addRow(['{0}', {1},'TEste']);", x.Key, x.Value));

            ////Opções adicionais do gráfico
            strGrafico.AppendLine("var options = { ");
            strGrafico.AppendFormat("title: '{0}',", tituloGrafico);
            strGrafico.AppendFormat("'width':{0},", height);
            strGrafico.AppendFormat("'height':{0},", width);
            strGrafico.AppendLine("enableInteractivity: false,");
            strGrafico.AppendLine("pieSliceTextStyle: {color: '#333',bold: true,fontSize:'12'},");
            strGrafico.AppendLine("chartArea: { left: 0, top: 0, width: '70%', height: '100%' },");
            strGrafico.AppendLine(@"'is3D':true,backgroundColor: { fill:'none' }");
            strGrafico.AppendLine("};");
            //Termina escript grafico



            ////Define onde será renderizado o gráfico,
            strGrafico.AppendFormat("var chart = new google.visualization.PieChart(document.getElementById('{0}'));", nomeGrafico);
            ////Renderiza o Gráfico com as opções adicionais definidas
            strGrafico.AppendLine("chart.draw(data, options);");



            //fim do escript
            strGrafico.AppendLine("}");
            strGrafico.AppendLine("</script>");

            return new HtmlString(strGrafico.ToString());
        }

        public static IHtmlString TemplateEmail(EmailChamadoViewModel model)
        {
            Builder = new StringBuilder();
            Builder.Append("<html><head></head>");
            Builder.Append("<body style='margin:0px;padding:0px;font-family:Cambria, Cochin, Georgia, Times, 'Times New Roman', serif'>");
            Builder.Append("<img src='http://infodesk.com.br/comunicado/mailing_top.jpg' alt='Infodesk Comunica'><br>");
            Builder.AppendFormat(
                "<h3 style='color:navy;margin-left:5px'>CHAMADO <span style='color:orange'>#{0}</span> ABERTO COM SUCESSO</h3>",
                model.CodigoChamado);
            Builder.Append("<ul style='list-style:none;padding:5px;color:navy'>");
            Builder.AppendFormat("<li>ASSUNTO :<span style='color:black'>{0}</span> </li>", model.Assunto);
            Builder.AppendFormat("<li>MODULO : <span style='color:black'>{0}</span></li>", model.Modulo);
            Builder.AppendFormat("<li>PRIORIDADE : <span style='color:black'>{0}</span></li>", model.Prioridade);
            Builder.AppendFormat("<li>ABERTO EM : <span style='color:black'>{0}</span></li>", model.DataAbertura);
            Builder.AppendFormat("<li>ATENDER ATÉ : <span style='color:black'>{0}</span> </li><br>",
                model.DataAbertura.AddDays(model.DiasAtender));
            Builder.AppendFormat(
                "<li>MENSAGEM :<div style='color:black;max-width:580px;background:#E6E6E6;padding:5px;text-align:justify'>{0}</div></li>",
                model.Mensagem);
            Builder.Append("</ul>");
            Builder.AppendFormat(
                "<a target='_blank' style='text-decoration:none;margin-left:5px;margin-bottom:5px;padding:15px;border-radius: 8px;;border:1px solid #333;font-weight:bold;background:rgb(25, 168, 206);color:#333;line-height:7px;float:left' href='http://suporte.infodesk.com.br/Chamado/Responder/{0}'>ACOMPANHAR CHAMADO </a><br>",
                model.CodigoChamado);
            Builder.Append("<div style='clear:both'></div>");
            Builder.Append("<img src='http://infodesk.com.br/comunicado/mailing_bottom.jpg' alt='Infodesk Comunica'>");
            Builder.Append("</body></html>");

            return new HtmlString(Builder.ToString());
        }

        public static IHtmlString TemplateEmailInfomarcaoStatus(string mensagem,string status,int codigoChamado)
        {
            Builder = new StringBuilder();
            Builder.Append("<html><head></head>");
            Builder.Append("<body style='margin:0px;padding:0px;font-family:Cambria, Cochin, Georgia, Times, 'Times New Roman', serif'>");
            Builder.Append("<img src='http://infodesk.com.br/comunicado/mailing_top.jpg' alt='Infodesk Comunica'><br>");
            Builder.AppendFormat(
                "<h3 style='color:navy;margin-left:5px'>CHAMADO <span style='color:orange'>#{0}</span> está {1} !</h3>",
               codigoChamado,status);
            Builder.Append("<ul style='list-style:none;padding:5px;color:navy'>");
            Builder.AppendFormat(
                          "<li>MENSAGEM :<div style='color:black;max-width:580px;background:#E6E6E6;padding:5px;text-align:justify'>{0}</div></li>",
                          mensagem);
            Builder.Append("</ul>");
            Builder.AppendFormat(
                "<a target='_blank' style='text-decoration:none;margin-left:5px;margin-bottom:5px;padding:15px;border-radius: 8px;;border:1px solid #333;font-weight:bold;background:rgb(25, 168, 206);color:#333;line-height:7px;float:left' href='http://suporte.infodesk.com.br/Chamado/Responder/{0}'>ACOMPANHAR CHAMADO </a><br>",
                codigoChamado);
            Builder.Append("<div style='clear:both'></div>");
            Builder.Append("<img src='http://infodesk.com.br/comunicado/mailing_bottom.jpg' alt='Infodesk Comunica'>");
            Builder.Append("</body></html>");

            return new HtmlString(Builder.ToString());
        }

        public static IHtmlString AutoCompleteList(this HtmlHelper html,AutoCompleteViewModel model)
        {
                

            Builder = new StringBuilder();
            Builder.AppendFormat("<select class='{0}' name='{0}' style='width: 220px !important;'>", model.Nome);
            Builder.AppendFormat("<option value='0'>{0}</option>", model.PlaceHolder);
            model.Items.ToList().ForEach(x => Builder.AppendFormat("<option value='{0}'>{1}</option>",x.Key,x.Value));
            Builder.AppendLine("</select>");

            //SCRIPT
            Builder.AppendLine("<script>");
            Builder.AppendLine("$(function () {");
            Builder.AppendFormat("$('.{0}').chosen({{", model.Nome);
            Builder.AppendLine("allow_single_deselect: true,");
            Builder.AppendLine("no_results_text: 'Nenhum resultado encontrado',");
            Builder.AppendLine("});");
            Builder.AppendLine("});");
            Builder.AppendLine("</script>");

            return  new HtmlString(Builder.ToString());
        }

    }
}

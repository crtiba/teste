//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GDESK.CORE.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Chamado
    {
        public Chamado()
        {
            this.Resposta = new HashSet<Resposta>();
            this.HistoricoChamado = new HashSet<HistoricoChamado>();
        }
    
        public int pk_int_chamado { get; set; }
        public System.DateTime dt_solicitacao { get; set; }
        public string vch_assunto { get; set; }
        public string vch_mensagem { get; set; }
        public int Usuario_pk_int_usuario { get; set; }
        public int Prioridade_pk_int_prioridade { get; set; }
        public int Usuario_pk_int_analista { get; set; }
        public int TipoChamado_pk_int_tipoChamado { get; set; }
        public int Status_pk_int_status { get; set; }
        public int Modulo_pk_int_modulo { get; set; }
        public Nullable<int> codChamadoOld { get; set; }
    
        public virtual Usuario Usuario { get; set; }
        public virtual Prioridade Prioridade { get; set; }
        public virtual Usuario Analista { get; set; }
        public virtual ICollection<Resposta> Resposta { get; set; }
        public virtual TipoChamado TipoChamado { get; set; }
        public virtual Status Status { get; set; }
        public virtual Modulo Modulo { get; set; }
        public virtual ICollection<HistoricoChamado> HistoricoChamado { get; set; }
    }
}

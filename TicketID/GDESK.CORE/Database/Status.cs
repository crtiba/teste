//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GDESK.CORE.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Status
    {
        public Status()
        {
            this.Chamado = new HashSet<Chamado>();
            this.HistoricoChamado = new HashSet<HistoricoChamado>();
        }
    
        public int pk_int_status { get; set; }
        public string vch_descricao { get; set; }
    
        public virtual ICollection<Chamado> Chamado { get; set; }
        public virtual ICollection<HistoricoChamado> HistoricoChamado { get; set; }
    }
}


-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/07/2014 13:17:11
-- Generated from EDMX file: X:\Publico\Projetos\GDESK\GDESK.CORE\Database\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [gdesk];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_EmpresaUsuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_EmpresaUsuario];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuarioChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_UsuarioChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_PrioridadeChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_PrioridadeChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_AnalistaChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_AnalistaChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_ChamadoResposta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Resposta] DROP CONSTRAINT [FK_ChamadoResposta];
GO
IF OBJECT_ID(N'[dbo].[FK_AnexoUsuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_AnexoUsuario];
GO
IF OBJECT_ID(N'[dbo].[FK_TipoChamadoChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_TipoChamadoChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_StatusChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_StatusChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuarioResposta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Resposta] DROP CONSTRAINT [FK_UsuarioResposta];
GO
IF OBJECT_ID(N'[dbo].[FK_ModuloChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chamado] DROP CONSTRAINT [FK_ModuloChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_ChamadoHistoricoChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoricoChamado] DROP CONSTRAINT [FK_ChamadoHistoricoChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_StatusHistoricoChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoricoChamado] DROP CONSTRAINT [FK_StatusHistoricoChamado];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuarioHistoricoChamado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoricoChamado] DROP CONSTRAINT [FK_UsuarioHistoricoChamado];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Usuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuario];
GO
IF OBJECT_ID(N'[dbo].[Empresa]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Empresa];
GO
IF OBJECT_ID(N'[dbo].[Modulo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Modulo];
GO
IF OBJECT_ID(N'[dbo].[Prioridade]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Prioridade];
GO
IF OBJECT_ID(N'[dbo].[Chamado]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Chamado];
GO
IF OBJECT_ID(N'[dbo].[Resposta]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Resposta];
GO
IF OBJECT_ID(N'[dbo].[Anexo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Anexo];
GO
IF OBJECT_ID(N'[dbo].[TipoChamado]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipoChamado];
GO
IF OBJECT_ID(N'[dbo].[Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Status];
GO
IF OBJECT_ID(N'[dbo].[HistoricoChamado]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistoricoChamado];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Usuario'
CREATE TABLE [dbo].[Usuario] (
    [pk_int_usuario] int IDENTITY(1,1) NOT NULL,
    [vch_nome] nvarchar(200)  NOT NULL,
    [vcn_login] nvarchar(50)  NOT NULL,
    [vch_senha] nvarchar(50)  NOT NULL,
    [bit_analista] bit  NOT NULL,
    [vch_email] nvarchar(200)  NOT NULL,
    [vc_celular] nvarchar(11)  NOT NULL,
    [Empresa_pk_int_empresa] int  NOT NULL,
    [Anexo_pk_int_anexo] int  NOT NULL
);
GO

-- Creating table 'Empresa'
CREATE TABLE [dbo].[Empresa] (
    [pk_int_empresa] int IDENTITY(1,1) NOT NULL,
    [vch_cnpj] nvarchar(18)  NOT NULL,
    [vch_nome] nvarchar(200)  NOT NULL,
    [bit_ativo] bit  NOT NULL
);
GO

-- Creating table 'Modulo'
CREATE TABLE [dbo].[Modulo] (
    [pk_int_modulo] int IDENTITY(1,1) NOT NULL,
    [vch_descricao] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'Prioridade'
CREATE TABLE [dbo].[Prioridade] (
    [pk_int_prioridade] int IDENTITY(1,1) NOT NULL,
    [vch_descricao] nvarchar(100)  NOT NULL,
    [int_DiasAtender] int  NOT NULL
);
GO

-- Creating table 'Chamado'
CREATE TABLE [dbo].[Chamado] (
    [pk_int_chamado] int IDENTITY(1,1) NOT NULL,
    [dt_solicitacao] datetime  NOT NULL,
    [vch_assunto] nvarchar(100)  NOT NULL,
    [vch_mensagem] nvarchar(max)  NOT NULL,
    [Usuario_pk_int_usuario] int  NOT NULL,
    [Prioridade_pk_int_prioridade] int  NOT NULL,
    [Usuario_pk_int_analista] int  NOT NULL,
    [TipoChamado_pk_int_tipoChamado] int  NOT NULL,
    [Status_pk_int_status] int  NOT NULL,
    [Modulo_pk_int_modulo] int  NOT NULL
);
GO

-- Creating table 'Resposta'
CREATE TABLE [dbo].[Resposta] (
    [pk_int_resposta] int IDENTITY(1,1) NOT NULL,
    [dt_postagem] datetime  NOT NULL,
    [vch_mensagem] nvarchar(max)  NOT NULL,
    [Chamado_pk_int_chamado] int  NOT NULL,
    [Usuario_pk_int_usuario] int  NOT NULL,
    [bit_privado] bit  NULL
);
GO

-- Creating table 'Anexo'
CREATE TABLE [dbo].[Anexo] (
    [pk_int_anexo] int IDENTITY(1,1) NOT NULL,
    [vch_fileName] nvarchar(100)  NOT NULL,
    [vch_pathName] nvarchar(200)  NOT NULL,
    [fk_int_chamado] int  NULL,
    [fk_int_resposta] int  NULL
);
GO

-- Creating table 'TipoChamado'
CREATE TABLE [dbo].[TipoChamado] (
    [pk_int_tipoChamado] int IDENTITY(1,1) NOT NULL,
    [vch_descricao] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Status'
CREATE TABLE [dbo].[Status] (
    [pk_int_status] int IDENTITY(1,1) NOT NULL,
    [vch_descricao] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HistoricoChamado'
CREATE TABLE [dbo].[HistoricoChamado] (
    [pk_int_historicoChamado] int IDENTITY(1,1) NOT NULL,
    [Chamado_pk_int_chamado] int  NOT NULL,
    [Status_pk_int_status] int  NOT NULL,
    [dt_De] datetime  NOT NULL,
    [dt_Ate] datetime  NULL,
    [Usuario_pk_int_Analista] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [pk_int_usuario] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [PK_Usuario]
    PRIMARY KEY CLUSTERED ([pk_int_usuario] ASC);
GO

-- Creating primary key on [pk_int_empresa] in table 'Empresa'
ALTER TABLE [dbo].[Empresa]
ADD CONSTRAINT [PK_Empresa]
    PRIMARY KEY CLUSTERED ([pk_int_empresa] ASC);
GO

-- Creating primary key on [pk_int_modulo] in table 'Modulo'
ALTER TABLE [dbo].[Modulo]
ADD CONSTRAINT [PK_Modulo]
    PRIMARY KEY CLUSTERED ([pk_int_modulo] ASC);
GO

-- Creating primary key on [pk_int_prioridade] in table 'Prioridade'
ALTER TABLE [dbo].[Prioridade]
ADD CONSTRAINT [PK_Prioridade]
    PRIMARY KEY CLUSTERED ([pk_int_prioridade] ASC);
GO

-- Creating primary key on [pk_int_chamado] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [PK_Chamado]
    PRIMARY KEY CLUSTERED ([pk_int_chamado] ASC);
GO

-- Creating primary key on [pk_int_resposta] in table 'Resposta'
ALTER TABLE [dbo].[Resposta]
ADD CONSTRAINT [PK_Resposta]
    PRIMARY KEY CLUSTERED ([pk_int_resposta] ASC);
GO

-- Creating primary key on [pk_int_anexo] in table 'Anexo'
ALTER TABLE [dbo].[Anexo]
ADD CONSTRAINT [PK_Anexo]
    PRIMARY KEY CLUSTERED ([pk_int_anexo] ASC);
GO

-- Creating primary key on [pk_int_tipoChamado] in table 'TipoChamado'
ALTER TABLE [dbo].[TipoChamado]
ADD CONSTRAINT [PK_TipoChamado]
    PRIMARY KEY CLUSTERED ([pk_int_tipoChamado] ASC);
GO

-- Creating primary key on [pk_int_status] in table 'Status'
ALTER TABLE [dbo].[Status]
ADD CONSTRAINT [PK_Status]
    PRIMARY KEY CLUSTERED ([pk_int_status] ASC);
GO

-- Creating primary key on [pk_int_historicoChamado] in table 'HistoricoChamado'
ALTER TABLE [dbo].[HistoricoChamado]
ADD CONSTRAINT [PK_HistoricoChamado]
    PRIMARY KEY CLUSTERED ([pk_int_historicoChamado] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Empresa_pk_int_empresa] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_EmpresaUsuario]
    FOREIGN KEY ([Empresa_pk_int_empresa])
    REFERENCES [dbo].[Empresa]
        ([pk_int_empresa])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpresaUsuario'
CREATE INDEX [IX_FK_EmpresaUsuario]
ON [dbo].[Usuario]
    ([Empresa_pk_int_empresa]);
GO

-- Creating foreign key on [Usuario_pk_int_usuario] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_UsuarioChamado]
    FOREIGN KEY ([Usuario_pk_int_usuario])
    REFERENCES [dbo].[Usuario]
        ([pk_int_usuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioChamado'
CREATE INDEX [IX_FK_UsuarioChamado]
ON [dbo].[Chamado]
    ([Usuario_pk_int_usuario]);
GO

-- Creating foreign key on [Prioridade_pk_int_prioridade] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_PrioridadeChamado]
    FOREIGN KEY ([Prioridade_pk_int_prioridade])
    REFERENCES [dbo].[Prioridade]
        ([pk_int_prioridade])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PrioridadeChamado'
CREATE INDEX [IX_FK_PrioridadeChamado]
ON [dbo].[Chamado]
    ([Prioridade_pk_int_prioridade]);
GO

-- Creating foreign key on [Usuario_pk_int_analista] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_AnalistaChamado]
    FOREIGN KEY ([Usuario_pk_int_analista])
    REFERENCES [dbo].[Usuario]
        ([pk_int_usuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AnalistaChamado'
CREATE INDEX [IX_FK_AnalistaChamado]
ON [dbo].[Chamado]
    ([Usuario_pk_int_analista]);
GO

-- Creating foreign key on [Chamado_pk_int_chamado] in table 'Resposta'
ALTER TABLE [dbo].[Resposta]
ADD CONSTRAINT [FK_ChamadoResposta]
    FOREIGN KEY ([Chamado_pk_int_chamado])
    REFERENCES [dbo].[Chamado]
        ([pk_int_chamado])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ChamadoResposta'
CREATE INDEX [IX_FK_ChamadoResposta]
ON [dbo].[Resposta]
    ([Chamado_pk_int_chamado]);
GO

-- Creating foreign key on [Anexo_pk_int_anexo] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_AnexoUsuario]
    FOREIGN KEY ([Anexo_pk_int_anexo])
    REFERENCES [dbo].[Anexo]
        ([pk_int_anexo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AnexoUsuario'
CREATE INDEX [IX_FK_AnexoUsuario]
ON [dbo].[Usuario]
    ([Anexo_pk_int_anexo]);
GO

-- Creating foreign key on [TipoChamado_pk_int_tipoChamado] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_TipoChamadoChamado]
    FOREIGN KEY ([TipoChamado_pk_int_tipoChamado])
    REFERENCES [dbo].[TipoChamado]
        ([pk_int_tipoChamado])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoChamadoChamado'
CREATE INDEX [IX_FK_TipoChamadoChamado]
ON [dbo].[Chamado]
    ([TipoChamado_pk_int_tipoChamado]);
GO

-- Creating foreign key on [Status_pk_int_status] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_StatusChamado]
    FOREIGN KEY ([Status_pk_int_status])
    REFERENCES [dbo].[Status]
        ([pk_int_status])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StatusChamado'
CREATE INDEX [IX_FK_StatusChamado]
ON [dbo].[Chamado]
    ([Status_pk_int_status]);
GO

-- Creating foreign key on [Usuario_pk_int_usuario] in table 'Resposta'
ALTER TABLE [dbo].[Resposta]
ADD CONSTRAINT [FK_UsuarioResposta]
    FOREIGN KEY ([Usuario_pk_int_usuario])
    REFERENCES [dbo].[Usuario]
        ([pk_int_usuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioResposta'
CREATE INDEX [IX_FK_UsuarioResposta]
ON [dbo].[Resposta]
    ([Usuario_pk_int_usuario]);
GO

-- Creating foreign key on [Modulo_pk_int_modulo] in table 'Chamado'
ALTER TABLE [dbo].[Chamado]
ADD CONSTRAINT [FK_ModuloChamado]
    FOREIGN KEY ([Modulo_pk_int_modulo])
    REFERENCES [dbo].[Modulo]
        ([pk_int_modulo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ModuloChamado'
CREATE INDEX [IX_FK_ModuloChamado]
ON [dbo].[Chamado]
    ([Modulo_pk_int_modulo]);
GO

-- Creating foreign key on [Chamado_pk_int_chamado] in table 'HistoricoChamado'
ALTER TABLE [dbo].[HistoricoChamado]
ADD CONSTRAINT [FK_ChamadoHistoricoChamado]
    FOREIGN KEY ([Chamado_pk_int_chamado])
    REFERENCES [dbo].[Chamado]
        ([pk_int_chamado])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ChamadoHistoricoChamado'
CREATE INDEX [IX_FK_ChamadoHistoricoChamado]
ON [dbo].[HistoricoChamado]
    ([Chamado_pk_int_chamado]);
GO

-- Creating foreign key on [Status_pk_int_status] in table 'HistoricoChamado'
ALTER TABLE [dbo].[HistoricoChamado]
ADD CONSTRAINT [FK_StatusHistoricoChamado]
    FOREIGN KEY ([Status_pk_int_status])
    REFERENCES [dbo].[Status]
        ([pk_int_status])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_StatusHistoricoChamado'
CREATE INDEX [IX_FK_StatusHistoricoChamado]
ON [dbo].[HistoricoChamado]
    ([Status_pk_int_status]);
GO

-- Creating foreign key on [Usuario_pk_int_Analista] in table 'HistoricoChamado'
ALTER TABLE [dbo].[HistoricoChamado]
ADD CONSTRAINT [FK_UsuarioHistoricoChamado]
    FOREIGN KEY ([Usuario_pk_int_Analista])
    REFERENCES [dbo].[Usuario]
        ([pk_int_usuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioHistoricoChamado'
CREATE INDEX [IX_FK_UsuarioHistoricoChamado]
ON [dbo].[HistoricoChamado]
    ([Usuario_pk_int_Analista]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
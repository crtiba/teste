﻿using System;
using System.Web.Mvc;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.UserControl;

namespace GDESK.CORE.Filter
{
    public class RestrigeChamadoFilter : ActionFilterAttribute
    {

        private readonly IChamadoService _chamadoService;

        public RestrigeChamadoFilter()
        {
            _chamadoService = new ChamadoService();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Suporte.UsuarioLogado.Analista) return;

            if(!filterContext.ActionParameters.ContainsKey("id")) throw new ArgumentNullException();

            var codigoChamado =  (int)filterContext.ActionParameters["id"];

            var chamado = _chamadoService.ObterChamadoEmpresa(Suporte.UsuarioLogado.CodigoEmpresa, codigoChamado);
            if (chamado != null) return;

            filterContext.Result = new RedirectResult("/Error/Ops");
        }
    }
}

﻿using System.Linq;
using System.Web.Mvc;
using GDESK.CORE.Enum;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.UserControl;

namespace GDESK.CORE.Filter
{
   

    public class TrocarStatusChamadoFilter:ActionFilterAttribute
    {
        private readonly IChamadoRepository _chamadoRepository;
        private readonly IChamadoService _chamadoService;

        public TrocarStatusChamadoFilter()
        {
            _chamadoRepository = new ChamadoRepository();
            _chamadoService = new ChamadoService();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (!Suporte.UsuarioLogado.Analista) return;
            var chamados =
                _chamadoRepository.ObterChamados(
                    x => x.Status_pk_int_status == (int)StatusChamadoEnum.Atendimento && x.Analista.pk_int_usuario == Suporte.UsuarioLogado.CodigoUsuario).ToList();

            if(chamados.Any())
                chamados.ForEach(x => _chamadoService.TrocarStatus(StatusChamadoEnum.Aguardando, x.pk_int_chamado, Suporte.UsuarioLogado.CodigoUsuario));
        }
    }
}

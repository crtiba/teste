﻿using System;
using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Enum;
using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Query;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public class TimeSheetService : ITimeSheetService
    {
        private readonly ITimeSheetRepository _sheetRepository;
        private readonly IMappingTimeSheet _mappingTimeSheet;

        public TimeSheetService()
        {
            _sheetRepository = new TimeSheetRepository();
            _mappingTimeSheet = new MappingTimeSheet();
        }

        public IEnumerable<TimeSheetViewModel> TimeSheetList(int codigoUsuario, int? codigoMes = null)
        {
            ITimeSheetQuery query = new TimeSheetQuery();

            var lst = query.Parameter(new TimeSheetFiltro { CodigoFuncionario = codigoUsuario, CodigoMes = codigoMes }).Execute().ToList();

            lst.ForEach(x => x.TotalHoras = CalcularSheet(x.EntradaPrimeiroPeriodo, x.SaidaPrimeiroPeriodo, x.EntradaSegundoPeriodo, x.SaidaSegundoPeriodo));

            return lst;
        }

        public void ApontarHora(TimeSheetViewModel model)
        {

            for (var diaApontamento = model.DiaDe; diaApontamento <= model.DiaAte; diaApontamento++)
            {
                model.DiaApontamento = diaApontamento;

                var date = new DateTime(model.AnoApontamento, model.MesApontamento, model.DiaApontamento);

                if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                    if (model.TipoHora == TipoHora.Normal)
                    continue;

                var timeSheet = _mappingTimeSheet.MappingTimeSheetViewModelToTimeSheet(model);

                _sheetRepository.Add(timeSheet);
                _sheetRepository.SaveChanges();
            }


        }

        public void AlterarStatus(ApontamentoViewModel[] apontamentos)
        {
            foreach (var ap in apontamentos)
            {
                var apontamento = _sheetRepository.Find(x => x.pk_int_timeSheet == ap.Codigo).First();
                apontamento.bit_aprovado = ap.Aprovado;
                _sheetRepository.SaveChanges();
            }
        }

        public string[] RecursosAguarandoAprovacao()
        {
            return _sheetRepository.Find(x => x.bit_aprovado == null).Select(x => x.Usuario.vch_nome).Distinct().ToArray();
        }

        private TimeSpan? CalcularSheet(TimeSpan? primeiraEntrada, TimeSpan? primeiraSaida, TimeSpan? segundaEntrada,
            TimeSpan? segundaSaida)
        {
            var totalTrabalhadoPrimeiroPeriodo = primeiraSaida - primeiraEntrada;
            var totalTrabalhadoSegundoPeriodo = segundaSaida - segundaEntrada;

            var timeSpan = totalTrabalhadoPrimeiroPeriodo + totalTrabalhadoSegundoPeriodo;
            return !timeSpan.HasValue ? null : timeSpan;
        }
    }
}

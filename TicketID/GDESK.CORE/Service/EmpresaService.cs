﻿using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IEmpresaRepository _empresaRepository;
        private readonly IMappingEmpresa _mappingEmpresa;

        public EmpresaService()
        {
            _empresaRepository = new EmpresaRepository();
            _mappingEmpresa = new MappingEmpresa();
        }

        public EmpresaViewModel VerificaEmpresa(string cnpj)
        {
            var empresa = _empresaRepository.VerificaExisteEmpresa(cnpj);
            return empresa == null ? null : _mappingEmpresa.MappingEmpresaToViewModelEmpresa(empresa);
        }

        public void AdicionarEmpresa(EmpresaViewModel model)
        {
            _empresaRepository.AdicionarEmpresa(_mappingEmpresa.MappingViewModelEmpresaToEmpresa(model));
        }
    }
}

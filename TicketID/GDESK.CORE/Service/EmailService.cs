﻿using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;
using System.Web;
using System.Net.Mail;
using System.Net;
using GDESK.CORE.Extension;
using System.Threading;
namespace GDESK.CORE.Service
{
    public class EmailService : IEmailService
    {
        private const string host = "smtp.gmail.com";
        private const int porta = 587;
        private const string usuario = "helpdesk@infodesk.com.br";
        private const string senha = "idadm12idadm56";

        private string _emailAssunto;

        private SmtpClient _smtpClient;
        private SmtpClient SmtpClient
        {
            get
            {
                return _smtpClient ?? (_smtpClient = new SmtpClient { Host = host, Port = porta, Credentials = new NetworkCredential(usuario, senha), EnableSsl = true });

            }
        }
        private MailMessage Email;


        private EmailChamadoViewModel _model;

        public IEmailService Model(EmailChamadoViewModel model)
        {
            _model = model;
            return this;
        }

        public IEmailService Assunto(string assuntoEmail)
        {
            _emailAssunto = assuntoEmail;
            return this;
        }

        public void AlterarStatusEmail(string mensagem, string status, int codigoChamado,string email)
        {
            new Thread(() =>
            {
              
                Email = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress(usuario),
                    Subject = string.Format("INFODESK - CHAMADO #{0}, FOI ALTERADO", codigoChamado),
                    Body = ExtensionHelper.TemplateEmailInfomarcaoStatus(mensagem, status, codigoChamado).ToString()
                };
               
                Email.To.Add(email);
                SmtpClient.Send(Email);
            }).Start();

        }

        public void Enviar()
        {
            new Thread(Execute).Start();
        }

        private void Execute()
        {
            Email = new MailMessage();
            Email.IsBodyHtml = true;
            Email.From = new MailAddress(usuario);
            Email.Subject = _emailAssunto;
            Email.Body = ExtensionHelper.TemplateEmail(_model).ToString();
            Email.To.Add(_model.Email);
            SmtpClient.Send(Email);
        }



    }
}

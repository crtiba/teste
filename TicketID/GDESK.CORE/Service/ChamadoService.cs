﻿using System;
using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Enum;
using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Query;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public class ChamadoService : IChamadoService
    {
        private readonly IMappingChamado _mappingChamado;
        private readonly IChamadoRepository _chamadoRepository;
        private readonly IInformacoesChamadoQuery _informacoesChamadoQuery;
        private readonly IRespostaRepository _respostaRepository;
        private readonly IMappingResposta _mappingResposta;
        private readonly IUsuarioService _usuarioService;
        private readonly IList<RespostaViewModel> _respostaViewModel;
        private readonly IHorario _horario;
        private readonly IAnexoService _anexoService;
        private readonly IHistoricoChamadoService _historicoChamadoService;
        private HistoricoChamadoViewModel _historicoChamadoViewModel;
        private readonly IInformacoesGraficoQuery _informacoesGraficoQuery;
        private readonly ITipoChamadoRepository _tipoChamadoRepository;
        private readonly IInformacoesEmailQuery _informacoesEmailQuery;
        private readonly IEmailService _emailService;


        private const string AssuntoEmailAbrir = "INFODESK - CHAMADO #{0} ABERTO COM SUCESSO";
        private const string AssuntoEmailFinalizado = "INFODESK - CHAMADO #{0} FOI FINALIZADO";

        public ChamadoService()
        {
            _mappingChamado = new MappingChamado();
            _chamadoRepository = new ChamadoRepository();
            _informacoesChamadoQuery = new InformacoesChamadoQuery();
            _respostaRepository = new RespostaRepository();
            _mappingResposta = new MappingResposta();
            _usuarioService = new UsuarioService();
            _horario = new Horario();
            _respostaViewModel = new List<RespostaViewModel>();
            _anexoService = new AnexoService();
            _historicoChamadoService = new HistoricoChamadoService();
            _horario = new Horario();
            _informacoesGraficoQuery = new InformacoesGraficoQuery();
            _tipoChamadoRepository = new TipoChamadoRepository();
            _informacoesEmailQuery = new InformacoesEmailQuery();
            _emailService = new EmailService();
        }

        public int AbrirChamado(ChamadoViewModel model)
        {

            var codigoChamdo = _chamadoRepository.AdicionarChamado(_mappingChamado.MappingChamadoViewModelToChamado(model));

            _emailService.Model(_informacoesEmailQuery.Paramenter(codigoChamdo).Execute()).Assunto(string.Format(AssuntoEmailAbrir, codigoChamdo)).Enviar();
            return codigoChamdo;
        }

        public void TrocarStatus(StatusChamadoEnum @enum, int codigoChamado, int? codigoAnalista = null)
        {
            _chamadoRepository.TrocarStatus((int)@enum, codigoChamado);

            if (!codigoAnalista.HasValue) return;

            if (_usuarioService.ObterUsuario(codigoAnalista.Value).Analista)
                _chamadoRepository.AtribuirAnalista(codigoChamado, codigoAnalista.Value);

            _historicoChamadoViewModel = new HistoricoChamadoViewModel
            {
                CodigoAnalista = codigoAnalista.Value,
                CodigoChamado = codigoChamado,
                CodigoStatus = (int)@enum,
                Ate = _horario.Agora(),
                De = _horario.Agora()
            };

            _historicoChamadoService.AtualizarHistorico(_historicoChamadoViewModel);


        }

        public StatusChamadoEnum ObterStatus(int codigoChamado)
        {
            var codigoStatus = _chamadoRepository.ObterStatusChamado(codigoChamado);

            switch (codigoStatus)
            {
                case (int)StatusChamadoEnum.Novo:
                    return StatusChamadoEnum.Novo;
                case (int)StatusChamadoEnum.Atendimento:
                    return StatusChamadoEnum.Atendimento;
                case (int)StatusChamadoEnum.Reaberto:
                    return StatusChamadoEnum.Reaberto;
                case (int)StatusChamadoEnum.Aguardando:
                    return StatusChamadoEnum.Aguardando;
                case (int)StatusChamadoEnum.Finalizado:
                    return StatusChamadoEnum.Finalizado;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public DetalhesChamadoViewModel ObterDetalhesChamado(int codigoChamado)
        {
            var deatlahes = _informacoesChamadoQuery.Parameter(codigoChamado).Execute();
            deatlahes.Items = _informacoesGraficoQuery.Parameter(codigoChamado).Execute();
            return deatlahes;
        }

        public ChamadoViewModel ObterChamado(int codigoChamado)
        {
            return _mappingChamado.MappingChamadoToChamadoViewMode(_chamadoRepository.ObterChamado(codigoChamado));
        }

        public ChamadoViewModel ObterChamadoEmpresa(int codigoEmpresa, int codigoChamado)
        {
            var chamado = _chamadoRepository.ObterChamado(x => x.Usuario.Empresa_pk_int_empresa == codigoEmpresa && x.pk_int_chamado == codigoChamado);
            return chamado == null ? null : _mappingChamado.MappingChamadoToChamadoViewMode(chamado);
        }

        public int AdicionarResposta(RespostaViewModel model)
        {
            model.DataResposta = _horario.Agora();

            return
                _respostaRepository.AdicionarResposta(_mappingResposta.MappingRespostaViewModelRespostaToResposta(model));
        }

        public IEnumerable<RespostaViewModel> ObterConversa(int codigoChamado)
        {
            _respostaRepository.ObterRespostasChamado(codigoChamado).ToList().ForEach(lst =>
            {
                var usuario = _usuarioService.ObterUsuario(lst.Usuario_pk_int_usuario);
                var anexos = _anexoService.ObterAnexos(lst.pk_int_resposta);

                _respostaViewModel.Add(new RespostaViewModel
                {
                    CodigoChamado = lst.Chamado_pk_int_chamado,
                    CodigoResposta = lst.pk_int_resposta,
                    CodigoUsuario = lst.Usuario_pk_int_usuario,
                    DataResposta = lst.dt_postagem,
                    Mensagem = lst.vch_mensagem,
                    Avatar = usuario.FileName,
                    NomeUsuario = usuario.Nome,
                    Anexos = anexos,
                    Privado = lst.bit_privado == true
                });
            });

            return _respostaViewModel;
        }

        public IEnumerable<AnexoViewModel> ObterAnexos(int codigoChamado)
        {
            return _anexoService.ObterAnexosCodigoChamado(codigoChamado);
        }

        public IEnumerable<RespostaViewModel> UltimasRespostas(int codigoChamado, int codigoResposta)
        {
            var respostas = _respostaRepository.ObterUltimasRespostas(codigoChamado, codigoResposta);


            if (respostas == null) return null;

            respostas.ToList().ForEach(lst =>
            {
                var usuario = _usuarioService.ObterUsuario(lst.Usuario_pk_int_usuario);
                var anexos = _anexoService.ObterAnexos(lst.pk_int_resposta);

                _respostaViewModel.Add(new RespostaViewModel
                {
                    CodigoChamado = lst.Chamado_pk_int_chamado,
                    CodigoResposta = lst.pk_int_resposta,
                    CodigoUsuario = lst.Usuario_pk_int_usuario,
                    DataResposta = lst.dt_postagem,
                    Mensagem = lst.vch_mensagem,
                    Avatar = usuario.FileName,
                    NomeUsuario = usuario.Nome,
                    Anexos = anexos,
                    Privado = lst.bit_privado == true
                });
            });

            return _respostaViewModel;
        }


        public UsuarioViewModel TrocarAnalista(int codigoChamado, int codigoAnalista)
        {
            _chamadoRepository.AtribuirAnalista(codigoChamado, codigoAnalista);
            return _usuarioService.ObterUsuario(codigoAnalista);
        }
    }
}

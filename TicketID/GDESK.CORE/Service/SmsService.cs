﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;
using HumanAPIClient.Model;
using HumanAPIClient.Service;

namespace GDESK.CORE.Service
{
    public class SmsService : ISmsService
    {
        private const string MensagemDefault = "{0}Chamado N: {1}, aberto com sucesso !";
        private const string De = "Suporte Infodesk";
        private const string Dd = "55";
        private readonly int _codigoChamado;
        private readonly SimpleMessage _simpleMessage;
        private readonly IList<SmsViewModel> _list;

        private static SimpleSending Sms { get { return new SimpleSending("infodesk.int", "HHhJpmCVvP"); } }
        

        public SmsService(int codigoChamado)
        {
            _codigoChamado = codigoChamado;
            _simpleMessage = new SimpleMessage();
            _list = new List<SmsViewModel>();
        }


        public ISmsService Adicionar(string celular)
        {
            var mensagem = string.Format(MensagemDefault, Environment.NewLine, _codigoChamado);
            var novoCell = celular.Length == 11 ? string.Format("{0}{1}", Dd, celular) : celular;

            _list.Add(new SmsViewModel { Celular = novoCell, De = De, Mensagem = mensagem });
            return this;
        }

        public void Enviar()
        {
            var thread = new Thread(Execute);
            thread.Start();
        }

        private void Execute()
        {
            _list.ToList().ForEach(x =>
            {
                _simpleMessage.From = x.De;
                _simpleMessage.Message = x.Mensagem;
                _simpleMessage.To = x.Celular;

                Sms.send(_simpleMessage);
            });
        }
    }
}

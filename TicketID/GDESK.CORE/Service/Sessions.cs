﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public static class Sessions
    {
      

        public static UsuarioViewModel UsuarioLogado
        {
            get
            {
                return
                    (HttpContext.Current.Session["UsuarioLogado"] ?? (HttpContext.Current.Session["UsuarioLogado"]  = null)) as
                        UsuarioViewModel;
            }
            set
            {
                HttpContext.Current.Session["UsuarioLogado"] = value;
            }
        }

        public static List<int> Abas
        {
            get
            {
                return
                    (HttpContext.Current.Session["Abas"] ?? (HttpContext.Current.Session["Abas"] = new List<int>())) as
                        List<int>;
            }
            set
            {
                HttpContext.Current.Session["Abas"] = value;
            }
        }

        public static List<GridChamadoViewModel> ListaChamado
        {
            get
            {
                return (HttpContext.Current.Session["ListaChamado"] ?? (new List<GridChamadoViewModel>())) as List<GridChamadoViewModel>;
            }
            set
            {
                HttpContext.Current.Session["ListaChamado"] = value;
            }

        }

        public static List<AnexoViewModel> Anexos
        {
            get
            {
                return (HttpContext.Current.Session["Anexos"] ?? (HttpContext.Current.Session["Anexos"] = new List<AnexoViewModel>())) as List<AnexoViewModel>;

            }
            set
            {
                HttpContext.Current.Session["Anexos"] = value;
            }

        }

    }
}

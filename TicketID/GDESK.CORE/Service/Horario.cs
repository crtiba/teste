﻿using System;
using GDESK.CORE.Service.Interface;

namespace GDESK.CORE.Service
{
    public class Horario : IHorario
    {
        public DateTime Agora()
        {
            return DateTime.Now;
        }
    }
}

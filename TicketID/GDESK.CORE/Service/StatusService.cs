﻿
using System;
using GDESK.CORE.Enum;
using GDESK.CORE.Service.Interface;

namespace GDESK.CORE.Service
{
    public class StatusService : IStatusService
    {
        public string ObterNomeStatus(StatusChamadoEnum @enum)
        {
            
            switch (@enum)
            {
                case StatusChamadoEnum.Novo:
                    return "Novo";
                case StatusChamadoEnum.Atendimento:
                    return "Em Atendimento";
                case StatusChamadoEnum.Reaberto:
                    return "Reaberto";
                case StatusChamadoEnum.Aguardando:
                    return "Aguardando";
                case StatusChamadoEnum.Finalizado:
                    return "Finalizado";
                default:
                    throw new ArgumentOutOfRangeException("enum");

            }
        }
    }
}

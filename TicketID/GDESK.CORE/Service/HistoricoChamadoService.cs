﻿using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public class HistoricoChamadoService : IHistoricoChamadoService
    {
        private readonly IHistoricoChamadoRepository _historicoChamadoRepository;
        private readonly IMappingHistoricoChamado _mappingHistoricoChamado;

        public HistoricoChamadoService()
        {
            _historicoChamadoRepository =  new HistoricoChamadoRepository();
            _mappingHistoricoChamado = new MappingHistoricoChamado();
        }

        public void AdicionarHistorico(HistoricoChamadoViewModel model)
        {
            model.Ate = null;
            _historicoChamadoRepository.Adicionar(_mappingHistoricoChamado.MappingHitoricoChamadoViewModelToHistoricoChamado(model));
        }

        public void AtualizarHistorico(HistoricoChamadoViewModel model)
        {

            _historicoChamadoRepository.Atualizar(_mappingHistoricoChamado.MappingHitoricoChamadoViewModelToHistoricoChamado(model));
            AdicionarHistorico(model);
        }
    }
}

﻿using System;

namespace GDESK.CORE.Service.Interface
{
    public interface IHorario
    {
        DateTime Agora();
    }
}

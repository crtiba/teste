﻿using System.Collections.Generic;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface ITimeSheetService
    {
        IEnumerable<TimeSheetViewModel> TimeSheetList(int codigoUsuario,int?codigoMes = null);

        void ApontarHora(TimeSheetViewModel model);

        void AlterarStatus(ApontamentoViewModel[] apontamentos);

        string[] RecursosAguarandoAprovacao();
    }
}

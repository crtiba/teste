﻿using GDESK.CORE.Enum;

namespace GDESK.CORE.Service.Interface
{
    public interface IStatusService
    {
        string ObterNomeStatus(StatusChamadoEnum @enum);
    }
}

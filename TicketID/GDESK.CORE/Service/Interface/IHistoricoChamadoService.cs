﻿using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface IHistoricoChamadoService
    {
        void AdicionarHistorico(HistoricoChamadoViewModel model);
        void AtualizarHistorico(HistoricoChamadoViewModel model);
    }
}

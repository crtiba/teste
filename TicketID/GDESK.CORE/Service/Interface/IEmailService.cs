﻿using GDESK.CORE.ViewModel.Control;
using System.Web;
namespace GDESK.CORE.Service.Interface
{
    public interface IEmailService
    {
        IEmailService Model(EmailChamadoViewModel model);
        IEmailService Assunto(string assuntoEmail);
        void AlterarStatusEmail(string mensagem, string status, int codigoChamado,string email);
        void Enviar();
    }
}

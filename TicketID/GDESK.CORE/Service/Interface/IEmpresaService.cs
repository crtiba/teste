﻿using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface IEmpresaService
    {
        EmpresaViewModel VerificaEmpresa(string cnpj);
        void AdicionarEmpresa(EmpresaViewModel model);
    }
}

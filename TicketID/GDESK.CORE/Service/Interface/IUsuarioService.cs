﻿using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface IUsuarioService
    {
        bool Autenticar(ViewModelLogin viewModelLogin);
        UsuarioViewModel ObterUsuario(int codigoUsuario);

        UsuarioViewModel ObterUsuarioPorCodigoChamado(int codigoChamado);

        UsuarioViewModel VerificaUsuario(string usuarioNome);
        int AdicionarUsuario(UsuarioViewModel usuario);
        void AlterarUsuario(UsuarioViewModel usuario);
        void Logout();
    }
}

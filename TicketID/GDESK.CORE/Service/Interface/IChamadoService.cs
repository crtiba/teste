﻿using System.Collections.Generic;
using GDESK.CORE.Enum;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface IChamadoService
    {
        int AbrirChamado(ChamadoViewModel model);
        void TrocarStatus(StatusChamadoEnum @enum, int codigoChamado,int? codigoAnalista = null);
        StatusChamadoEnum ObterStatus(int codigoChamado);
        DetalhesChamadoViewModel ObterDetalhesChamado(int codigoChamado);
        ChamadoViewModel ObterChamado(int codigoChamado);
        ChamadoViewModel ObterChamadoEmpresa(int codigoEmpresa, int codigoChamado);
        int AdicionarResposta(RespostaViewModel model);

        IEnumerable<RespostaViewModel> ObterConversa(int codigoChamado);
        IEnumerable<AnexoViewModel> ObterAnexos(int codigoChamado);
        IEnumerable<RespostaViewModel> UltimasRespostas(int codigoChamado, int codigoResposta);
        UsuarioViewModel TrocarAnalista(int codigoChamado, int CodigoAnalista);
    }
}

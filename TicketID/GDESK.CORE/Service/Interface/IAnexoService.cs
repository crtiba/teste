﻿using System.Collections.Generic;
using System.Web;
using GDESK.CORE.Enum;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service.Interface
{
    public interface IAnexoService
    {
        IAnexoService Arquivo(HttpPostedFileBase file);
        IAnexoService FileName(string fileName);
        IAnexoService Deletar(string fileName);

        IAnexoService Anexar(DestinoAnexoEnum destino);
        IEnumerable<AnexoViewModel> ObterAnexos(int codResposta);
        IEnumerable<AnexoViewModel> ObterAnexosCodigoChamado(int codChamado);
        int Adicionar(AnexoViewModel model);

        AnexoViewModel Anexo(int codUsuario);


    }
}

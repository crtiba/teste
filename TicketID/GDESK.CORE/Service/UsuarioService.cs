﻿using System;
using System.Web.Security;
using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;
using System.Text.RegularExpressions;

namespace GDESK.CORE.Service
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMappingUsuario _mappingUsuario;
        private readonly IEmpresaRepository _empresaRepository;
        private readonly IChamadoRepository _chamadoRepository;
        private readonly IAnexoRepository _anexoRepository;

        public string GuidLogin {
            get { return new Guid().ToString();}
        }


        public UsuarioService()
        {
            _usuarioRepository = new UsuarioRepository();
            _mappingUsuario = new MappingUsuario();
            _empresaRepository = new EmpresaRepository();
            _anexoRepository = new AnexoRepository();
            _chamadoRepository = new ChamadoRepository();
        }

        public bool Autenticar(ViewModelLogin viewModelLogin)
        {
            var usuario = _usuarioRepository.VerificarPorLoginSenha(viewModelLogin.Login, viewModelLogin.Senha);
            if(usuario == null) return false;


            usuario.Empresa = _empresaRepository.ObterEmpresa(usuario.Empresa_pk_int_empresa);
            usuario.Anexo = _anexoRepository.ObterAnexo(usuario.Anexo_pk_int_anexo);

            FormsAuthentication.SetAuthCookie(GuidLogin,true);
            Sessions.UsuarioLogado = _mappingUsuario.MappingUsuaroToViewModelUsuario(usuario);
            return true;

        }

        public UsuarioViewModel ObterUsuario(int codigoUsuario)
        {
            return _mappingUsuario.MappingUsuaroToViewModelUsuario(_usuarioRepository.ObterUsuario(codigoUsuario));
        }

        public UsuarioViewModel ObterUsuarioPorCodigoChamado(int codigoChamado)
        {
            return _mappingUsuario.MappingUsuaroToViewModelUsuario(_chamadoRepository.ObterUsuarioPorCodigoChamado(codigoChamado));
        }

        public int AdicionarUsuario(UsuarioViewModel usuario)
        {
            usuario.Celular = Regex.Replace(usuario.Celular, @"[^\d]", "");
            return _usuarioRepository.AdicionarUsuario(_mappingUsuario.MappingViewModelUsuarioToUsuario(usuario));
        }

        public void AlterarUsuario(UsuarioViewModel usuario)
        {
            usuario.Celular = Regex.Replace(usuario.Celular, @"[^\d]", "");
            _usuarioRepository.Atualizar(_mappingUsuario.MappingViewModelUsuarioToUsuario(usuario));
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
            Sessions.UsuarioLogado = null;
        }



        public UsuarioViewModel VerificaUsuario(string usuarioNome)
        {
            var usuario = _usuarioRepository.ObterUsuario(model => model.vcn_login == usuarioNome);

            return usuario ==  null ? null :  _mappingUsuario.MappingUsuaroToViewModelUsuario(usuario);
        }
    }
}

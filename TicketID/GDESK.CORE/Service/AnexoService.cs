﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using GDESK.CORE.Enum;
using GDESK.CORE.Mapping;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Service
{
    public class AnexoService : IAnexoService
    {

        private readonly IMappingAnexo _mappingAnexo;
        private readonly IAnexoRepository _anexoRepository;

        private HttpPostedFileBase _file { get; set; }
        private string _fileName { get; set; }
        public DestinoAnexoEnum DestinoAnexoEnum { get; set; }

        public AnexoService()
        {
            _mappingAnexo = new MappingAnexo();
            _anexoRepository = new AnexoRepository();
        }


        public IAnexoService Arquivo(HttpPostedFileBase file)
        {
            _file = file;
            return this;
        }
        public IAnexoService FileName(string fileName)
        {
            _fileName = fileName;
            return this;
        }

        public IAnexoService Deletar(string fileName)
        {
            if(File.Exists(fileName))
                File.Delete(fileName);
            return this;
        }

        public IAnexoService Anexar(DestinoAnexoEnum destino)
        {
            Execute();
            
            return this;
        }

        public IEnumerable<AnexoViewModel> ObterAnexos(int codResposta)
        {
            return _anexoRepository.ObterAnexos(codResposta).Select(x => new AnexoViewModel
            {
                CaminhoArquivo = x.vch_pathName,
                CodigoAnexo = x.pk_int_anexo,
                CodigoChamado = x.fk_int_chamado,
                CodigoResposta = x.fk_int_resposta,
                NomeArquivo = x.vch_fileName
            });
        }

        public IEnumerable<AnexoViewModel> ObterAnexosCodigoChamado(int codChamado)
        {
            return _anexoRepository.ObterAnexosCodigoChamado(codChamado).Select(x => new AnexoViewModel
            {
                CaminhoArquivo = x.vch_pathName,
                CodigoAnexo = x.pk_int_anexo,
                CodigoChamado = x.fk_int_chamado,
                CodigoResposta = x.fk_int_resposta,
                NomeArquivo = x.vch_fileName
            });
        }

        public int Adicionar(AnexoViewModel model)
        {
           return _anexoRepository.Adicionar(_mappingAnexo.MappingAnexoViewModelToAnexto(model));
        }

        public AnexoViewModel Anexo(int codAnexo)
        {
            return _mappingAnexo.MappingAnextoToAnexoViewModel(_anexoRepository.ObterAnexo(codAnexo));
        }

        private void Execute()
        {
            _file.SaveAs(_fileName);
        }
    }
}

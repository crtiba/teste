﻿using GDESK.CORE.ViewModel.Page;
using System.Collections.Generic;
namespace GDESK.CORE.ViewModel.Control
{
    public class TrocarAnalistaViewModel
    {
        public int CodigoAnalista { get; set; }
        public string NomeAnalista { get; set; }
        public IEnumerable<int?> Chamados { get; set; }
    }
}

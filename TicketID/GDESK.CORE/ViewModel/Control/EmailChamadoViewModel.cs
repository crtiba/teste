﻿using System;

namespace GDESK.CORE.ViewModel.Control
{
    public class EmailChamadoViewModel
    {
        public string Email { get; set; }
        
        public int CodigoChamado { get; set; }
        public string Assunto { get; set; }
        public string Modulo { get; set; }
        public string Prioridade { get; set; }
        public DateTime DataAbertura { get; set; }
        public int DiasAtender { get; set; }
        public string Mensagem { get; set; }
    }
}

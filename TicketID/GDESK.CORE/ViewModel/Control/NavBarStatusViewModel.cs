﻿namespace GDESK.CORE.ViewModel.Control
{
    public class NavBarStatusViewModel
    {
        public bool Novo { get; set; }
        public bool Atendimento { get; set; }
        public bool Reaberto { get; set; }
        public bool Aguardando { get; set; }
        public bool Finalizado { get; set; }

        public int CodigoChamado { get; set; }
        public string Analista { get; set; }
    }
}

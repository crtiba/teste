﻿namespace GDESK.CORE.ViewModel.Control
{
    public class ViewModelLogin
    {
        public string Login { get; set; }
        public string Senha { get; set; }

        public string UrlDefault { get; set; }
    }
}

﻿namespace GDESK.CORE.ViewModel.Control
{
    public class ChamadoHeaderViewModel
    {
        public int Alta { get; set; }
        public int Media { get; set; }
        public int Baixa { get; set; }
    }
}

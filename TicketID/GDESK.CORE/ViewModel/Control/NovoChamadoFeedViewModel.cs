﻿namespace GDESK.CORE.ViewModel.Control
{
    public class NovoChamadoFeedViewModel
    {
        public int CodigoChamado { get; set; }
        public string Solicitante { get; set; }
        public string AvatarSolicitante { get; set; }
        public string Assunto { get; set; }
        public string Mensagem { get; set; }
    }
}

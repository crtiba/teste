﻿using System;
using System.Collections.Generic;

namespace GDESK.CORE.ViewModel.Control
{
    public class DetalhesChamadoViewModel
    {
        //DETALHES DO CHAMADO
        public string NomeAnalista { get; set; }
        public int CodigoAnalista { get; set; }
        public string Modulo { get; set; }
        public string Status { get; set; }
        public string Prioridade { get; set; }
        public DateTime DataAbertura { get; set; }
        public int CodigoChamado { get; set; }
        

       //DETALHES DO CLIENTE
        public string NomeEmpresa { get; set; }
        public string NomeFuncionario { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string FoneEmpresa { get; set; }

        //GRAFICO
        public IList<GraficoViewModel> Items { get; set; }

    }
}

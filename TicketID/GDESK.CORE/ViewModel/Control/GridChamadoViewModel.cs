﻿using System;

namespace GDESK.CORE.ViewModel.Control
{
    public class GridChamadoViewModel
    {
        public int CodigoChamado { get; set; }
        public string NomeCliente { get; set; }
        public string NomeEmpresa { get; set; }
        public string Assunto { get; set; }
        public DateTime DataAbertura { get; set; }
        public string NomeAnalista { get; set; }
        public int AtenderEm { get; set; }
        public DateTime PrazoAtendimento
        {
            get
            {
                return DataAbertura.AddDays(AtenderEm);
            }
        }

        public long Sticks {
            get
            {
                return (PrazoAtendimento.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            }
        }
        public string Prioridade { get; set; }
        public string Modulo { get; set; }
        public string Status { get; set; }

        public int CodigoEmpresa { get; set; }
        public int CodigoAnalista { get; set; }
        public int CodigoStatus { get; set; }

        public int QtdResposta { get; set; }
    }
}

﻿using System.Web;
using GDESK.CORE.Enum;

namespace GDESK.CORE.ViewModel.Control
{
    public class UploadViewModel
    {
        private string FilePath { get { return "~/AppData/"; }}
        public string FileName { get; set; }
        private HttpPostedFileBase Anexo { get; set; }
        public DestinoAnexoEnum DestinoAnexo { get; set; }
        public int Codigo { get; set; }
    }
}

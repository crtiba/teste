﻿namespace GDESK.CORE.ViewModel.Control
{
    public class FiltroChamdoViewModel
    {
        public bool MostrarChamadoFechados { get; set; }

        public int CodigoChamado { get; set; }
        public int CodigoEmpresa { get; set; }
        public int CodigoAnalista { get; set; }
        public int CodigoStatus { get; set; }
        public bool Fechados { get; set; }
    }
}

﻿namespace GDESK.CORE.ViewModel.Control
{
    public class SelecionarClienteViewModel
    {
        public int CodigoEmpresa { get; set; }
        public int CodigoSolicitante { get; set; }
    }
}

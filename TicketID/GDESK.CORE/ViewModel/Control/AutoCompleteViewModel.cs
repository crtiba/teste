﻿using System.Collections.Generic;

namespace GDESK.CORE.ViewModel.Control
{
    public class AutoCompleteViewModel
    {
        public string Nome { get; set; }

        public string PlaceHolder { get; set; }
        public Dictionary<int, string> Items { get; set; }
    }
}

﻿using System;
using System.Web.Mvc;

namespace GDESK.CORE.ViewModel.Page
{
    public class ChamadoViewModel
    {
        public int CodigoChamado { get; set; }
        public string NomeSolicitante { get; set; }
        public string Sobrenome { get; set; }
        public DateTime DataAbertura
        {
            get { return DateTime.Now; }
        }

        public int CodigoModulo { get; set; }
        public int CodigoPrioridade { get; set; }
        public int CodigoTipoChamado { get; set; }
        public int CodigoSolicitante { get; set; }
        public int CodigoAnalista { get; set; }
        public int CodigoStatus { get; set; }

        public string Assunto { get; set; }
        [AllowHtml]
        public string Mensagem { get; set; }

        public UsuarioViewModel Usuario { get; set; }

    }

}

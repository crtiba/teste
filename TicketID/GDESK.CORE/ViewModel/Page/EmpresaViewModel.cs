﻿namespace GDESK.CORE.ViewModel.Page
{
    public class EmpresaViewModel
    {
        public int CodigoEmpresa { get; set; }
        public string Cnpj { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
    }
}

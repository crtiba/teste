﻿namespace GDESK.CORE.ViewModel.Page
{
    public class AnexoViewModel
    {
        public int CodigoAnexo { get; set; }
        public int? CodigoChamado { get; set; }
        public int? CodigoResposta { get; set; }
        public string NomeArquivo { get; set; }
        public string CaminhoArquivo { get; set; }
    }
}

﻿using System;
using GDESK.CORE.Enum;

namespace GDESK.CORE.ViewModel.Page
{
    public class TimeSheetViewModel
    {
        public int Codigo { get; set; }
        public int CodigoFuncionario { get; set; }
        public string NomeFuncionario { get; set; }
        public int DiaDe { get; set; }
        public int DiaAte { get; set; }
        public int DiaApontamento { get; set; }
        public int AnoApontamento { get; set; }
        public int MesApontamento { get; set; }
        public TipoHora TipoHora { get; set; }
        public TimeSpan? EntradaPrimeiroPeriodo { get; set; }
        public TimeSpan? SaidaPrimeiroPeriodo { get; set; }
        public TimeSpan? EntradaSegundoPeriodo { get; set; }
        public TimeSpan? SaidaSegundoPeriodo { get; set; }
        public TimeSpan? TotalHoras { get; set; }
        public int? CodigoProjeto { get; set; }
        public string DescricaoProjeto { get; set; }
        public bool? Aprovado { get; set; }
        public string Status { get; set; }

    }

    public class TimeSheetFiltro
    {
        public int CodigoFuncionario { get; set; }
        public int? CodigoMes { get; set; }
    }

    public class ApontamentoViewModel
    {
        public int Codigo { get; set; }
        public int CodigoUsuario { get; set; }
        public bool Aprovado { get; set; }

    }
}

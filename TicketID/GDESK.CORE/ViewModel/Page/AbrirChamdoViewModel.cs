﻿using System;

namespace GDESK.CORE.ViewModel.Page
{
    public class AbrirChamdoViewModel
    {
        public string NomeEmpresa { get; set; }
        public string PrimeiroNome { get; set; }
        public string Sobrenome { get; set; }
        public DateTime DataAbertura {
            get { return DateTime.Now; }
        }

        public int CodigoModulo { get; set; }
        public int CodigoPrioridade { get; set; }
        public int CodigoTipoChamado { get; set; }

        public string Assunto { get; set; }
        public string Mensagem { get; set; }

    }
}

﻿namespace GDESK.CORE.ViewModel.Page
{
    public class UsuarioViewModel
    {
        public int CodigoUsuario { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public bool Analista { get; set; }
        public string Email { get; set; }
        public int CodigoEmpresa { get; set; }
        public string Empresa { get; set; }
        public string FileName { get; set; }
        public string Celular { get; set; }
        public int CodigoAnexo { get; set; }
        public bool Gerente { get; set; }
    }
}

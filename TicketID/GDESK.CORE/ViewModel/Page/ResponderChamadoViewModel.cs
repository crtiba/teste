﻿using System.Collections.Generic;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.ViewModel.Page
{
    public class ResponderChamadoViewModel
    {
        public UsuarioViewModel Usuario { get; set; }
        public ChamadoViewModel Chamado { get; set; }
        public DetalhesChamadoViewModel DetalhesChamadoViewModel { get; set; }
        public IEnumerable<AnexoViewModel> Anexos { get; set; }
        public List<RespostaViewModel> Conversas { get; set; }
    }
}

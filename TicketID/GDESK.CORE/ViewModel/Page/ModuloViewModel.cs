﻿namespace GDESK.CORE.ViewModel.Page
{
    public class ModuloViewModel
    {
        public int CodigoModulo { get; set; }
        public string Descricao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GDESK.CORE.ViewModel.Page
{
    public class RespostaViewModel
    {
        public int CodigoResposta { get; set; }
        public int CodigoUsuario { get; set; }
        public int CodigoChamado { get; set; }

        public DateTime DataResposta { get; set; }
        [AllowHtml]
        public string Mensagem { get; set; }

        public Boolean Privado { get; set; }

        public string Avatar { get; set; }
        public string NomeUsuario { get; set; }


        public IEnumerable<AnexoViewModel> Anexos { get; set; }
    }
}

﻿namespace GDESK.CORE.ViewModel.Page
{
    public class PrioridadeViewModel
    {
        public int CodigoPrioridade { get; set; }
        public string Descricao { get; set; }
        public int DiasAtender { get; set; }
    }
}

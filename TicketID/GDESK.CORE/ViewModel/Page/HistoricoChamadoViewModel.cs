﻿using System;

namespace GDESK.CORE.ViewModel.Page
{
    public class HistoricoChamadoViewModel
    {
        public int CodigoHistoricoChamado { get; set; }
        public int CodigoChamado { get; set; }
        public int CodigoStatus { get; set; }
        public int CodigoAnalista { get; set; }
        public DateTime De { get; set; }
        public DateTime? Ate { get; set; }
    }
}

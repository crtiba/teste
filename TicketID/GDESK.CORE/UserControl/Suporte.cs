﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.Service;
using GDESK.CORE.ViewModel.Control;
using GDESK.CORE.ViewModel.Page;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Query;
using System.IO;

namespace GDESK.CORE.UserControl
{
    public static class Suporte
    {
        private static readonly IStatusRepository StatusRepository = new StatusRepository();
        private static readonly IUsuarioRepository UsuarioRepository = new UsuarioRepository();
        private static readonly IEmpresaRepository EmpresaRepository = new EmpresaRepository();
        private static readonly IModuloRepository ModuloRepository = new ModuloRepository();
        private static readonly IPrioridadeRepository PrioridadeRepository = new PrioridadeRepository();
        private static readonly ITipoChamadoRepository TipoChamadoRepository = new TipoChamadoRepository();
        private static readonly ITrocarAnalistaListaQuery TrocarAnalistaListaQuery = new TrocarAnalistaListaQuery();

        public static UsuarioViewModel UsuarioLogado
        {
            get
            {
              
                return Sessions.UsuarioLogado;
            }
        }

        public static List<int> Abas
        {
            get
            {
                return Sessions.Abas;
            }
            set
            {
                Sessions.Abas = value;
            }
        }


        #region DROPDOWN
        public static Dictionary<int, string> DropDownEmpresa()
        {
            return EmpresaRepository.ObterTodas().Where(x => x.bit_ativo).ToDictionary(x => x.pk_int_empresa, x => x.vch_nome);
        }

        public static Dictionary<int, string> DropDownAnalista()
        {
            return UsuarioRepository.ObterAnalistas().ToDictionary(x => x.pk_int_usuario, x => x.vch_nome);
        }

        public static Dictionary<int, string> DropDownStatus()
        {
            return StatusRepository.ObterTodos().ToDictionary(x => x.pk_int_status, x => x.vch_descricao);
        }

        public static Dictionary<int, string> DropDownPaginacao()
        {
            return new Dictionary<int, string> { { 30, "Paginar em 30" }, { 40, "Paginar em 40" } };
        }
        #endregion

        #region GRAFICOS
        public static Dictionary<object, object> ConfigurarLabelGraficoChamado()
        {


            return new Dictionary<object, object> { { "string", "Status" }, { "number", "Tempo Atendimento" } };
        }

        public static Dictionary<string, int> ConfiguararDadosGraficoChamado(IList<GraficoViewModel> model)
        {
            var dictionary = new Dictionary<string, int>();

            model.ToList().ForEach(x =>
            {
                var tempo = TimeSpan.FromSeconds(x.Segundos);
                dictionary.Add(string.Format("{0} - {1}d{2}h{3}m{4}s", x.Descricao, tempo.Days, tempo.Hours, tempo.Minutes, tempo.Seconds), x.Segundos);

            });

            return dictionary;



        }
        #endregion

        #region SELECTLIST

        public static Dictionary<int, string> SelectModulo()
        {
            return ModuloRepository.ObterTodos().ToDictionary(x => x.pk_int_modulo, x => x.vch_descricao);
        }

        public static Dictionary<int, string> SelectPrioridade()
        {
            return PrioridadeRepository.ObterTodas().ToDictionary(x => x.pk_int_prioridade, x => x.vch_descricao);
        }

        public static Dictionary<int, string> SelectTipoChamado()
        {
            return TipoChamadoRepository.ObterTodas().ToDictionary(x => x.pk_int_tipoChamado, x => x.vch_descricao);
        }

        public static IEnumerable<TrocarAnalistaViewModel> TrocarAnalistaLista()
        {
            return TrocarAnalistaListaQuery.Execute();
        }

        #endregion



    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class StatusRepository : Repositorio<Status>, IStatusRepository 
    {
        public IStatusRepository TrocarStatus(int codigoChamado, int codigoStatus)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Status> ObterTodos()
        {
            return GetAll();
        }
    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class AnexoRepository : Repositorio<Anexo>, IAnexoRepository
    {
        public int Adicionar(Anexo anexo)
        {
            Add(anexo);
            SaveChanges();
            return anexo.pk_int_anexo;
        }

        public IEnumerable<Anexo> ObterAnexos(int codResposta)
        {
            return Find(x => x.fk_int_resposta == codResposta);
        }

        public IEnumerable<Anexo> ObterAnexosCodigoChamado(int codChamado)
        {
            return Find(x => x.fk_int_chamado == codChamado);
        }

        public Anexo ObterAnexo(int codAnexo)
        {
            return First(x => x.pk_int_anexo == codAnexo);
        }

    }
}

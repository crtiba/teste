﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class PrioridadeRepository : Repositorio<Prioridade>, IPrioridadeRepository
    {
        public IEnumerable<Prioridade> ObterTodas()
        {
            return GetAll();
        }

        public int ObterDiasAtendimento(int codigoPrioridade)
        {
            return First(x => x.pk_int_prioridade == codigoPrioridade).int_DiasAtender;
        }
    }
}

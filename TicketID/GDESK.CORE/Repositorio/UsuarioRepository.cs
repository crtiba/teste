﻿using System;
using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class UsuarioRepository : Repositorio<Usuario>, IUsuarioRepository
    {

        public int AdicionarUsuario(Usuario usuario)
        {
           
            try
            {
                Add(usuario);
                SaveChanges();
                return usuario.pk_int_usuario;
            }
            catch
            {
                return 0;
            }
        }

        public Usuario VerificarPorLoginSenha(string login, string senha)
        {
            return First(x => x.vcn_login.Equals(login) && x.vch_senha.Equals(senha));
        }

        public Usuario ObterUsuario(int codigoUsuario)
        {
            return First(x => x.pk_int_usuario == codigoUsuario);
        }

        public IEnumerable<Usuario> ObterAnalistas()
        {
            return Find(x => x.bit_analista);
        }

        public IEnumerable<Usuario> ObterUsuarios(Func<Usuario, bool> predicate)
        {
           return Find(predicate);
        }

        public void Atualizar(Usuario usuario)
        {
            var user = First(x => x.pk_int_usuario == usuario.pk_int_usuario);
            if (user == null) return; 

            user.vch_nome = usuario.vch_nome;
            user.vc_celular = usuario.vc_celular;
            user.vch_email = usuario.vch_email;
            user.vch_senha = usuario.vch_senha;
            user.Anexo_pk_int_anexo = usuario.Anexo_pk_int_anexo;

            SaveChanges();
        }

        public Usuario ObterUsuario(System.Func<Usuario, bool> predicate)
        {
            return First(predicate);
        }


    }
}

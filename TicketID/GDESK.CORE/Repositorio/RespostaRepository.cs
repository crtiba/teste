﻿using System;
using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class RespostaRepository :Repositorio<Resposta>, IRespostaRepository
    {
        public int AdicionarResposta(Resposta resposta)
        {
            Add(resposta);
            SaveChanges();
            return resposta.pk_int_resposta;
        }

        public IEnumerable<Resposta> ObterRespostasChamado(int codigoChamado)
        {
            return Find(x => x.Chamado_pk_int_chamado == codigoChamado);
        }

        public IEnumerable<Resposta> ObterUltimasRespostas(int codigoChamado, int codigoResposta)
        {
            return Find(x => x.Chamado_pk_int_chamado == codigoChamado && x.pk_int_resposta > codigoResposta);
        }
    }
}

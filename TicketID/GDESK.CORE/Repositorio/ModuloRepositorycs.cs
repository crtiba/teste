﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    class ModuloRepository : Repositorio<Modulo>, IModuloRepository
    {
        public IEnumerable<Modulo> ObterTodos()
        {
            return GetAll();
        }
    }
}

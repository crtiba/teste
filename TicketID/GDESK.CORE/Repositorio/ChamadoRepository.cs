﻿using System;
using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class ChamadoRepository : Repositorio<Chamado>, IChamadoRepository
    {
        public int AdicionarChamado(Chamado chamado)
        {
            Add(chamado);
            SaveChanges();

            return chamado.pk_int_chamado;
        }

        public void TrocarStatus(int codigoStatus, int codigoChamado)
        {
            var chamado = First(x => x.pk_int_chamado == codigoChamado);
            chamado.Status_pk_int_status = codigoStatus;
            SaveChanges();
        }

        public Usuario ObterUsuarioPorCodigoChamado(int codigoChamado)
        {
            return First(x => x.pk_int_chamado.Equals(codigoChamado)).Usuario;
        }

        public int ObterStatusChamado(int codigoChamdo)
        {
            return First(x => x.pk_int_chamado == codigoChamdo).Status_pk_int_status;
        }

        public Chamado ObterChamado(int codigoChamado)
        {
            return First(x => x.pk_int_chamado == codigoChamado);
        }

        public Chamado ObterChamado(Func<Chamado, bool> predicate)
        {
            return First(predicate);
        }

        public void AtribuirAnalista(int codigoChamado,int codigoAnalista)
        {
            var chamado = First(x => x.pk_int_chamado == codigoChamado);
            if (chamado == null) return;
            chamado.Usuario_pk_int_analista = codigoAnalista;
            SaveChanges();
        }

        public IEnumerable<Chamado> ObterChamados(Func<Chamado, bool> predicate)
        {
            return Fetch().Where(predicate);
        }
    }
}

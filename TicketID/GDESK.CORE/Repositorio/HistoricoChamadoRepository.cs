﻿using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class HistoricoChamadoRepository : Repositorio<HistoricoChamado>, IHistoricoChamadoRepository
    {
        public void Adicionar(HistoricoChamado model)
        {
           
            Add(model);
            SaveChanges();
        }

        public void Atualizar(HistoricoChamado model)
        {
            var historico = First(x => x.Chamado_pk_int_chamado == model.Chamado_pk_int_chamado && x.dt_Ate == null);
            if (historico == null) return;
            historico.dt_Ate = model.dt_Ate;
            SaveChanges();


        }
    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class EmpresaRepository :Repositorio<Empresa> ,IEmpresaRepository
    {
        public void AdicionarEmpresa(Empresa empresa)
        {
            Add(empresa);
            SaveChanges();
        }

        public Empresa VerificaExisteEmpresa(string cnfpj)
        {
            return First(x => x.vch_cnpj == cnfpj);
        }

        public Empresa ObterEmpresa(int codigoEmpresa)
        {
            return First(x => x.pk_int_empresa == codigoEmpresa);
        }

        public IEnumerable<Empresa> ObterTodas()
        {
            return GetAll();
        }
    }
}

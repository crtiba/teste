﻿using GDESK.CORE.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GDESK.CORE.Repositorio.Base
{
    public abstract class Repositorio<T> : IRepositorio<T>, IDisposable
        where T : class
    {

        private bool _disposed;
        private readonly DbContext _dbContext;
        private readonly DbSet<T> _dbSet;

        protected Repositorio()
            : this(DbContextFactory.Instance)
        {
        }

        ~Repositorio()
        {
            Dispose(false);
        }

        private Repositorio(DbContextFactory context)
        {
            _dbContext = context.GetOrCreateContext();
            _dbSet = _dbContext.Set<T>();
            

        }

        public IQueryable<T> Fetch()
        {
            _dbContext.ChangeTracker.DetectChanges();
            return _dbSet;
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.AsEnumerable();
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).AsEnumerable();
        }

        public T Single(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).SingleOrDefault();
        }

        public T First(Func<T, bool> predicate)
        {
            return _dbSet.Where(predicate).FirstOrDefault();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void Attach(T entity)
        {
            _dbSet.Attach(entity);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _dbContext.Dispose();
            }
            _disposed = true;
        }
    }
}

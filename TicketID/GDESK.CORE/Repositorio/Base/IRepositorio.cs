﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GDESK.CORE.Repositorio
{
    public interface IRepositorio<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Fetch();
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);
        TEntity Single(Func<TEntity, bool> predicate);
        TEntity First(Func<TEntity, bool> predicate);
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Attach(TEntity entity);
        void SaveChanges();

    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using GDESK.CORE.Repositorio.Base;
using GDESK.CORE.Repositorio.Interface;

namespace GDESK.CORE.Repositorio
{
    public class TipoChamadoRepository : Repositorio<TipoChamado>, ITipoChamadoRepository
    {
        public IEnumerable<TipoChamado> ObterTodas()
        {
            return GetAll();
        }
    }
}

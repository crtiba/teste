﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IEmpresaRepository
    {
        void AdicionarEmpresa(Empresa empresa);
        Empresa VerificaExisteEmpresa(string cnfpj);
        Empresa ObterEmpresa(int codigoEmpresa);
        IEnumerable<Empresa> ObterTodas();
    }
}

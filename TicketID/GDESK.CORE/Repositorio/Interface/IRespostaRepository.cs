﻿using System;
using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IRespostaRepository
    {
        int AdicionarResposta(Resposta resposta);
        IEnumerable<Resposta> ObterRespostasChamado(int codigoChamado);
        IEnumerable<Resposta> ObterUltimasRespostas(int codigoChamado, int codigoResposta);
    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface ITipoChamadoRepository
    {
        IEnumerable<TipoChamado> ObterTodas();
    }
}

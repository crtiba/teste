﻿using System;
using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IChamadoRepository
    {
        int AdicionarChamado(Chamado chamado);
        void TrocarStatus(int codigoStatus, int codigoChamado);

        Usuario ObterUsuarioPorCodigoChamado(int codigoChamado);

        int ObterStatusChamado(int codigoChamdo);
        Chamado ObterChamado(int codigoChamado);
        Chamado ObterChamado(Func<Chamado,bool> predicate);
        void AtribuirAnalista(int codigoChamado,int codigoAnalista);

        IEnumerable<Chamado> ObterChamados(Func<Chamado, bool> predicate);

    }
}

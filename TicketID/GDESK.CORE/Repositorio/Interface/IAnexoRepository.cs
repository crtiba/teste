﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IAnexoRepository
    {
        int Adicionar(Anexo anexo);
        IEnumerable<Anexo> ObterAnexos(int codResposta);
        IEnumerable<Anexo> ObterAnexosCodigoChamado(int codChamado);
        Anexo ObterAnexo(int codAnexo);
    }
}

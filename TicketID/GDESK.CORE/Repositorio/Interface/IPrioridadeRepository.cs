﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IPrioridadeRepository
    {
        IEnumerable<Prioridade> ObterTodas();
        int ObterDiasAtendimento(int codigoPrioridade);
    }
}

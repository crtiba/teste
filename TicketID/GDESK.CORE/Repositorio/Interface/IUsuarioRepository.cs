﻿using System.Collections.Generic;
using GDESK.CORE.Database;
using System;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IUsuarioRepository
    {
        int AdicionarUsuario(Usuario usuario);
        Usuario VerificarPorLoginSenha(string login, string senha);
        Usuario ObterUsuario(int codigoUsuario);
        Usuario ObterUsuario(Func<Usuario,bool> predicate);
        IEnumerable<Usuario> ObterAnalistas();
        IEnumerable<Usuario> ObterUsuarios(Func<Usuario,bool> predicate);
        void Atualizar(Usuario usuario);
    }
}

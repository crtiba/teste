﻿using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IHistoricoChamadoRepository
    {
        void Adicionar(HistoricoChamado model);
        void Atualizar(HistoricoChamado model);
    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IModuloRepository
    {
        IEnumerable<Modulo> ObterTodos();
    }
}

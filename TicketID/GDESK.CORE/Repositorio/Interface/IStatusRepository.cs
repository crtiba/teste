﻿using System.Collections.Generic;
using GDESK.CORE.Database;

namespace GDESK.CORE.Repositorio.Interface
{
    public interface IStatusRepository
    {
        IStatusRepository TrocarStatus(int codigoChamado, int codigoStatus);
        IEnumerable<Status> ObterTodos();
    }
}

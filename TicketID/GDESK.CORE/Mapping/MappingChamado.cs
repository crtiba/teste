﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingChamado : IMappingChamado
    {
        public Chamado MappingChamadoViewModelToChamado(ChamadoViewModel model)
        {
            return new Chamado
            {
               Modulo_pk_int_modulo = model.CodigoModulo,
               Prioridade_pk_int_prioridade = model.CodigoPrioridade,
               dt_solicitacao = model.DataAbertura,
               vch_mensagem = model.Mensagem,
               vch_assunto = model.Assunto,
               TipoChamado_pk_int_tipoChamado = model.CodigoTipoChamado,
               Usuario_pk_int_analista = model.CodigoAnalista,
               Usuario_pk_int_usuario = model.CodigoSolicitante,
               Status_pk_int_status = model.CodigoStatus,
            };
        }

        public ChamadoViewModel MappingChamadoToChamadoViewMode(Chamado model)
        {
            return new ChamadoViewModel
            {
                CodigoChamado = model.pk_int_chamado,
                Assunto = model.vch_assunto,
                CodigoModulo = model.Modulo_pk_int_modulo,
                CodigoPrioridade = model.Prioridade_pk_int_prioridade,
                CodigoSolicitante = model.Usuario_pk_int_usuario,
                Mensagem = model.vch_mensagem,
                CodigoTipoChamado = model.TipoChamado_pk_int_tipoChamado,
                CodigoAnalista = model.Usuario_pk_int_analista,
                CodigoStatus = model.Status_pk_int_status
                
            };
        }
    }
}

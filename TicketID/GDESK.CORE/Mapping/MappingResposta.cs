﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingResposta : IMappingResposta
    {
        public Resposta MappingRespostaViewModelRespostaToResposta(RespostaViewModel model)
        {
            return new Resposta
            {
                Chamado_pk_int_chamado = model.CodigoChamado,
                dt_postagem = model.DataResposta,
                pk_int_resposta = model.CodigoResposta,
                vch_mensagem = model.Mensagem,
                Usuario_pk_int_usuario = model.CodigoUsuario,
                bit_privado = model.Privado
            };
        }

        public RespostaViewModel MappingRespostaRespostaToViewModelResposta(Resposta model)
        {
            return new RespostaViewModel
            {
                CodigoChamado = model.Chamado_pk_int_chamado,
                CodigoResposta = model.pk_int_resposta,
                DataResposta = model.dt_postagem,
                CodigoUsuario = model.Usuario_pk_int_usuario,
                Mensagem = model.vch_mensagem,
                Privado = model.bit_privado != null
                
            };
        }
    }
}

﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingAnexo : IMappingAnexo
    {
        public AnexoViewModel MappingAnextoToAnexoViewModel(Anexo model)
        {
            return new AnexoViewModel
            {
                CaminhoArquivo = model.vch_pathName,
                CodigoAnexo = model.pk_int_anexo,
                CodigoChamado = model.fk_int_chamado,
                CodigoResposta = model.fk_int_resposta,
                NomeArquivo = model.vch_fileName
            };
        }

        public Anexo MappingAnexoViewModelToAnexto(AnexoViewModel model)
        {
            return new Anexo
            {
                fk_int_chamado = model.CodigoChamado,
                fk_int_resposta = model.CodigoResposta,
                pk_int_anexo = model.CodigoAnexo,
                vch_fileName = model.NomeArquivo,
                vch_pathName = model.CaminhoArquivo
            };
        }
    }
}

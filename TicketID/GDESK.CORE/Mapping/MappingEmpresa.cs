﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingEmpresa : IMappingEmpresa
    {
        public Empresa MappingViewModelEmpresaToEmpresa(EmpresaViewModel empresaViewModel)
        {
            return new Empresa
            {
                bit_ativo = empresaViewModel.Ativo,
                pk_int_empresa = empresaViewModel.CodigoEmpresa,
                vch_cnpj = empresaViewModel.Cnpj,
                vch_nome = empresaViewModel.Nome
                
            };
        }

        public EmpresaViewModel MappingEmpresaToViewModelEmpresa(Empresa empresa)
        {
            return new EmpresaViewModel
            {
                Ativo = empresa.bit_ativo,
                Cnpj = empresa.vch_cnpj,
                CodigoEmpresa = empresa.pk_int_empresa,
                Nome = empresa.vch_nome
            };
        }
    }
}

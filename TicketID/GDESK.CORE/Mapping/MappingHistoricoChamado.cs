﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingHistoricoChamado : IMappingHistoricoChamado
    {
        public virtual HistoricoChamado MappingHitoricoChamadoViewModelToHistoricoChamado(HistoricoChamadoViewModel model)
        {
            return new HistoricoChamado
            {
                pk_int_historicoChamado = model.CodigoHistoricoChamado,
                Chamado_pk_int_chamado = model.CodigoChamado,
                Status_pk_int_status = model.CodigoStatus,
                Usuario_pk_int_Analista = model.CodigoAnalista,
                dt_Ate = model.Ate,
                dt_De = model.De
            };
        }

        public virtual HistoricoChamadoViewModel MappingHistoricoChamadoToHitoricoChamadoViewModel(HistoricoChamado model)
        {
            return new HistoricoChamadoViewModel
            {
                CodigoAnalista = model.Usuario_pk_int_Analista,
                CodigoChamado = model.Chamado_pk_int_chamado,
                CodigoStatus = model.Status_pk_int_status,
                Ate = model.dt_Ate,
                De = model.dt_De,
                CodigoHistoricoChamado = model.pk_int_historicoChamado
               
            };
        }
    }
}

﻿using System;
using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    class MappingTimeSheet:IMappingTimeSheet
    {
        public TimeSheet MappingTimeSheetViewModelToTimeSheet(TimeSheetViewModel model)
        {
            return new TimeSheet
            {
                dt_entradaPrimeiroPeriodo = model.EntradaPrimeiroPeriodo,
                dt_saidaPrimeiroPeriodo = model.SaidaPrimeiroPeriodo,
                dt_entradaSegundoPeriodo = model.EntradaSegundoPeriodo,
                dt_saidaSegundoPeriodo = model.SaidaSegundoPeriodo,
                fk_codProjeto = model.CodigoProjeto,
                fk_int_codFuncionario = model.CodigoFuncionario,
                int_dia = model.DiaApontamento,
                int_tipoHora = (int)model.TipoHora,
                pk_int_timeSheet = model.Codigo,
                vch_descricaoProjeto = model.DescricaoProjeto,
                int_ano = model.AnoApontamento,
                int_mes = model.MesApontamento,
            };
        }

        public ChamadoViewModel MappingTimeSheetToTimeSheetViewModel(TimeSheet model)
        {
            throw new NotImplementedException();
        }
    }
}

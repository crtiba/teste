﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingAnexo
    {
        AnexoViewModel MappingAnextoToAnexoViewModel(Anexo model);
        Anexo MappingAnexoViewModelToAnexto(AnexoViewModel model);
    }
}

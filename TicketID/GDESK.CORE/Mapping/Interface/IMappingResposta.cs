﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingResposta
    {
        Resposta MappingRespostaViewModelRespostaToResposta(RespostaViewModel model);
        RespostaViewModel MappingRespostaRespostaToViewModelResposta(Resposta model);
    }
}

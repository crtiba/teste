﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingChamado
    {
        Chamado MappingChamadoViewModelToChamado(ChamadoViewModel model);
        ChamadoViewModel MappingChamadoToChamadoViewMode(Chamado model);
    }
}

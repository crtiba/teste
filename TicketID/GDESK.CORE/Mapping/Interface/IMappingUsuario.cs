﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingUsuario
    {
        Usuario MappingViewModelUsuarioToUsuario(UsuarioViewModel usuarioViewModel);
        UsuarioViewModel MappingUsuaroToViewModelUsuario(Usuario usuario);
    }
}

﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingEmpresa
    {
        Empresa MappingViewModelEmpresaToEmpresa(EmpresaViewModel empresaViewModel);
        EmpresaViewModel MappingEmpresaToViewModelEmpresa(Empresa empresa);
    }
}

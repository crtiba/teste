﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingTimeSheet
    {
        TimeSheet MappingTimeSheetViewModelToTimeSheet(TimeSheetViewModel model);
        ChamadoViewModel MappingTimeSheetToTimeSheetViewModel(TimeSheet model);
    }

}

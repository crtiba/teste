﻿using GDESK.CORE.Database;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping.Interface
{
    public interface IMappingHistoricoChamado
    {
        HistoricoChamado MappingHitoricoChamadoViewModelToHistoricoChamado(HistoricoChamadoViewModel model);
        HistoricoChamadoViewModel MappingHistoricoChamadoToHitoricoChamadoViewModel(HistoricoChamado model);
    }
}

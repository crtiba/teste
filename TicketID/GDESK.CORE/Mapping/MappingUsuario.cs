﻿using GDESK.CORE.Database;
using GDESK.CORE.Mapping.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Mapping
{
    public class MappingUsuario:IMappingUsuario
    {
        public Usuario MappingViewModelUsuarioToUsuario(UsuarioViewModel usuarioViewModel)
        {
            return new Usuario
            {
                pk_int_usuario = usuarioViewModel.CodigoUsuario,
                Empresa_pk_int_empresa = usuarioViewModel.CodigoEmpresa,
                vch_email = usuarioViewModel.Email,
                vcn_login = usuarioViewModel.Login,
                vch_senha = usuarioViewModel.Senha,
                vch_nome = usuarioViewModel.Nome,
                bit_analista = usuarioViewModel.Analista,
                Anexo_pk_int_anexo = usuarioViewModel.CodigoAnexo,
                vc_celular = usuarioViewModel.Celular,
                bit_gerente = usuarioViewModel.Gerente,
                

            };
        }

        public UsuarioViewModel MappingUsuaroToViewModelUsuario(Usuario usuario)
        {
            return new UsuarioViewModel
            {
                Analista = usuario.bit_analista,
                CodigoEmpresa = usuario.Empresa_pk_int_empresa,
                CodigoUsuario = usuario.pk_int_usuario,
                Email = usuario.vch_email,
                Login = usuario.vcn_login,
                Nome = usuario.vch_nome,
                Senha = usuario.vch_senha,
                FileName = usuario.Anexo.vch_pathName,
                Empresa = usuario.Empresa.vch_nome,
                Celular = usuario.vc_celular,
                CodigoAnexo = usuario.Anexo_pk_int_anexo,
                Gerente = usuario.bit_gerente == true
            };
        }
    }
}

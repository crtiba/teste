﻿using GDESK.CORE.Database;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Control;
using System.Linq;

namespace GDESK.CORE.Query
{
    public class InformacoesEmailQuery : IInformacoesEmailQuery
    {

        private int _codigoChamado;

        public IInformacoesEmailQuery Paramenter(int codigoChamado)
        {
            _codigoChamado = codigoChamado;
            return this;
        }

        public EmailChamadoViewModel Execute()
        {
            using (var context = new ModelContainer())
            {

                var model =  (from chamado in context.Chamado
                        join modulo in context.Modulo
                            on chamado.Modulo_pk_int_modulo equals modulo.pk_int_modulo
                        join prioridade in context.Prioridade
                        on chamado.Prioridade_pk_int_prioridade equals prioridade.pk_int_prioridade
                        join usuario in context.Usuario on chamado.Usuario_pk_int_usuario equals usuario.pk_int_usuario
                        where chamado.pk_int_chamado == _codigoChamado
                        select new EmailChamadoViewModel
                        {
                            Assunto = chamado.vch_assunto,
                            CodigoChamado = chamado.pk_int_chamado,
                            DataAbertura = chamado.dt_solicitacao,
                            DiasAtender = prioridade.int_DiasAtender,
                            Mensagem = chamado.vch_mensagem,
                            Modulo = modulo.vch_descricao,
                            Prioridade = prioridade.vch_descricao,
                            Email = usuario.vch_email

                        }).FirstOrDefault();

                return model;

            }
        }
    }
}

﻿using GDESK.CORE.Database;
using GDESK.CORE.Enum;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Control;
using System.Collections.Generic;
using System.Linq;
namespace GDESK.CORE.Query
{

    public class TrocarAnalistaListaQuery : ITrocarAnalistaListaQuery
    {

        public IList<TrocarAnalistaViewModel> Execute()
        {
            using (var context = new ModelContainer())
            {

                var model = (from usuario in context.Usuario
                             join chamado in context.Chamado
                                 on usuario.pk_int_usuario equals chamado.Usuario_pk_int_analista into chamadoGrp
                             from chamado in chamadoGrp.DefaultIfEmpty()
                             where usuario.pk_int_usuario != (int)AnalistaEnum.Padrao && usuario.bit_analista 
                             group chamado by new { usuario.pk_int_usuario, usuario.vch_nome } into grp
                             select new TrocarAnalistaViewModel
                             {
                                 NomeAnalista = grp.Key.vch_nome,
                                 CodigoAnalista = grp.Key.pk_int_usuario,
                                 Chamados = grp.Where(x => x.Status_pk_int_status != (int)StatusChamadoEnum.Finalizado).Select(x => (int?)x.pk_int_chamado)

                             }).ToList();

                return model;
            }
        }


    }
}

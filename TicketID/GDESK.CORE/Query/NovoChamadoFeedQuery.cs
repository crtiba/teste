﻿using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query
{
    public class NovoChamadoFeedQuery : INovoChamadoFeedQuery
    {
        private int _codigoSolicitante;

        public INovoChamadoFeedQuery Parameter(int codigoSolicitante)
        {
            _codigoSolicitante = codigoSolicitante;
            return this;
        }

        public NovoChamadoFeedViewModel Execute()
        {
            using (var context = new ModelContainer())
            {
                return (from usuario in context.Usuario
                        join chamado in context.Chamado on
                            usuario.pk_int_usuario equals chamado.Usuario_pk_int_usuario
                        join anexo in context.Anexo on usuario.Anexo_pk_int_anexo equals anexo.pk_int_anexo
                        where usuario.pk_int_usuario == _codigoSolicitante
                        select new NovoChamadoFeedViewModel
                        {
                            Assunto = chamado.vch_assunto,
                            CodigoChamado = chamado.pk_int_chamado,
                            Mensagem = chamado.vch_mensagem,
                            Solicitante = usuario.vch_nome,
                            AvatarSolicitante = anexo.vch_pathName

                        }).FirstOrDefault();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Enum;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Query
{
    public class TimeSheetQuery : ITimeSheetQuery
    {

        private TimeSheetFiltro _parameter;

        public ITimeSheetQuery Parameter(TimeSheetFiltro paramter)
        {
            _parameter = paramter;
            return this;
        }

        public IEnumerable<TimeSheetViewModel> Execute()
        {
            try
            {
                using (var context = new ModelContainer())
                {
                    var query = context.TimeSheet.Join(context.Usuario, timeSheet => timeSheet.fk_int_codFuncionario,
                        usuario => usuario.pk_int_usuario, (timeSheet, usuario) => new { timeSheet, usuario })
                        .Where(model => model.usuario.pk_int_usuario == _parameter.CodigoFuncionario && model.timeSheet.int_mes == _parameter.CodigoMes)
                        .Select(model => new TimeSheetViewModel
                        {
                            Codigo = model.timeSheet.pk_int_timeSheet,
                            CodigoFuncionario = model.usuario.pk_int_usuario,
                            CodigoProjeto = model.timeSheet.fk_codProjeto,
                            DescricaoProjeto = model.timeSheet.vch_descricaoProjeto,
                            DiaApontamento =  model.timeSheet.int_dia ?? 0,
                            AnoApontamento = model.timeSheet.int_ano ?? 0,
                            MesApontamento = model.timeSheet.int_mes ?? 0,
                            EntradaPrimeiroPeriodo = model.timeSheet.dt_entradaPrimeiroPeriodo,
                            EntradaSegundoPeriodo = model.timeSheet.dt_entradaSegundoPeriodo,
                            NomeFuncionario = model.usuario.vch_nome,
                            SaidaPrimeiroPeriodo = model.timeSheet.dt_saidaPrimeiroPeriodo,
                            SaidaSegundoPeriodo = model.timeSheet.dt_saidaSegundoPeriodo,
                            TipoHora = (TipoHora)model.timeSheet.int_tipoHora,
                            Aprovado = model.timeSheet.bit_aprovado,
                            Status = model.timeSheet.bit_aprovado == null ? "Aguardando" : model.timeSheet.bit_aprovado.Value ? "Aprovado" : "Reprovada"

                        });

                    return query.OrderBy(x=>x.DiaApontamento).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

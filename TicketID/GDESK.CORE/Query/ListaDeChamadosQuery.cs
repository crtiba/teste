﻿using System.Collections.Generic;
using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Enum;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Repositorio;
using GDESK.CORE.Repositorio.Interface;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query
{
    public class ListaDeChamadosQuery : IListaDeChamadosQuery
    {
        private FiltroChamdoViewModel _filtroChamdoViewModel;
        private IQueryable<GridChamadoViewModel> _queryable;
        private readonly IPrioridadeRepository _prioridadeRepository;

        public ListaDeChamadosQuery()
        {
            _prioridadeRepository = new PrioridadeRepository();
        }

        public IListaDeChamadosQuery Paramter(FiltroChamdoViewModel parameter = null)
        {
            _filtroChamdoViewModel = parameter;
            return this;
        }

        public IEnumerable<GridChamadoViewModel> Execute()
        {
            using (var context = new ModelContainer())
            {
                _queryable = (context.Chamado.Join(context.Modulo, chamado => chamado.Modulo_pk_int_modulo,
                    modulo => modulo.pk_int_modulo, (chamado, modulo) => new { chamado, modulo })
                    .Join(context.Prioridade, @t => @t.chamado.Prioridade_pk_int_prioridade,
                        prioridade => prioridade.pk_int_prioridade, (@t, prioridade) => new { @t, prioridade })
                    .Join(context.Status, @t => @t.@t.chamado.Status_pk_int_status, status => status.pk_int_status,
                        (@t, status) => new { @t, status })
                    .Join(context.TipoChamado, @t => @t.@t.@t.chamado.TipoChamado_pk_int_tipoChamado,
                        tipoChamado => tipoChamado.pk_int_tipoChamado, (@t, tipoChamado) => new { @t, tipoChamado })
                    .Join(context.Usuario, @t => @t.@t.@t.@t.chamado.Usuario_pk_int_usuario,
                        usuario => usuario.pk_int_usuario, (@t, usuario) => new { @t, usuario })
                    .Join(context.Usuario, @t => @t.@t.@t.@t.@t.chamado.Usuario_pk_int_analista,
                        analista => analista.pk_int_usuario, (@t, analista) => new { @t, analista })
                    .Join(context.Empresa, @t => @t.@t.usuario.Empresa_pk_int_empresa,
                        empresa => empresa.pk_int_empresa, (@t, empresa) => new GridChamadoViewModel
                        {
                            Assunto = @t.@t.@t.@t.@t.@t.chamado.vch_assunto,
                            CodigoAnalista = @t.analista.pk_int_usuario,
                            CodigoChamado = @t.@t.@t.@t.@t.@t.chamado.pk_int_chamado,
                            CodigoEmpresa = empresa.pk_int_empresa,
                            CodigoStatus = @t.@t.@t.@t.status.pk_int_status,
                            DataAbertura = @t.@t.@t.@t.@t.@t.chamado.dt_solicitacao,
                            Modulo = @t.@t.@t.@t.@t.@t.modulo.vch_descricao,
                            NomeAnalista = @t.analista.vch_nome,
                            NomeCliente = @t.@t.usuario.vch_nome,
                            NomeEmpresa = empresa.vch_nome,
                            Prioridade = @t.@t.@t.@t.@t.prioridade.vch_descricao,
                            Status = @t.@t.@t.@t.status.vch_descricao,
                            AtenderEm = @t.@t.@t.@t.@t.prioridade.int_DiasAtender,
                            QtdResposta = @t.@t.@t.@t.@t.@t.chamado.Resposta.Count
                        }));


                if (_filtroChamdoViewModel == null) return _queryable.ToList();

                if (_filtroChamdoViewModel.CodigoChamado > 0)
                    _queryable = _queryable.Where(x => x.CodigoChamado.Equals(_filtroChamdoViewModel.CodigoChamado));
                if (_filtroChamdoViewModel.CodigoAnalista > 0)
                    _queryable = _queryable.Where(x => x.CodigoAnalista.Equals(_filtroChamdoViewModel.CodigoAnalista));
                if (_filtroChamdoViewModel.CodigoEmpresa > 0)
                    _queryable = _queryable.Where(x => x.CodigoEmpresa.Equals(_filtroChamdoViewModel.CodigoEmpresa));
                if (_filtroChamdoViewModel.CodigoStatus > 0)
                    _queryable = _queryable.Where(x => x.CodigoStatus.Equals(_filtroChamdoViewModel.CodigoStatus));
                if (!_filtroChamdoViewModel.Fechados)
                    _queryable = _queryable.Where(x => x.CodigoStatus != (int) StatusChamadoEnum.Finalizado);
                

                return _queryable.ToList();
            }
        }
    }
}

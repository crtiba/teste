﻿using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query
{
    public class InformacoesChamadoQuery : IInformacoesChamadoQuery
    {
        private int _codigoChamado;
        private DetalhesChamadoViewModel _chamadoViewModel;

        public IInformacoesChamadoQuery Parameter(int codigoChamado)
        {
            _codigoChamado = codigoChamado;
            return this;
        }

        public DetalhesChamadoViewModel Execute()
        {
            using (var context = new ModelContainer())
            {
                _chamadoViewModel = (context.Chamado.Join(context.Modulo, chamado => chamado.Modulo_pk_int_modulo,
                    modulo => modulo.pk_int_modulo, (chamado, modulo) => new {chamado, modulo})
                    .Join(context.Status, @t => @t.chamado.Status_pk_int_status, status => status.pk_int_status,
                        (@t, status) => new {@t, status})
                    .Join(context.Prioridade, @t => @t.@t.chamado.Prioridade_pk_int_prioridade,
                        prioridade => prioridade.pk_int_prioridade, (@t, prioridade) => new {@t, prioridade})
                    .Join(context.Usuario, @t => @t.@t.@t.chamado.Usuario_pk_int_usuario,
                        usuario => usuario.pk_int_usuario, (@t, usuario) => new {@t, usuario})
                    .Join(context.Empresa, @t => @t.usuario.Empresa_pk_int_empresa, empresa => empresa.pk_int_empresa,
                        (@t, empresa) => new {@t, empresa})
                    .Join(context.Usuario, @t => @t.@t.@t.@t.@t.chamado.Usuario_pk_int_analista,
                        analista => analista.pk_int_usuario, (@t, analista) => new {@t, analista})
                    .Where(@t => @t.@t.@t.@t.@t.@t.chamado.pk_int_chamado == _codigoChamado)
                    .Select(@t => new DetalhesChamadoViewModel
                    {
                        NomeEmpresa = @t.@t.empresa.vch_nome,
                        DataAbertura = @t.@t.@t.@t.@t.@t.chamado.dt_solicitacao,
                        CodigoChamado = @t.@t.@t.@t.@t.@t.chamado.pk_int_chamado,
                        NomeAnalista = @t.analista.vch_nome,
                        CodigoAnalista = @t.analista.pk_int_usuario,
                        Celular = @t.@t.@t.usuario.vc_celular,
                        Modulo = @t.@t.@t.@t.@t.@t.modulo.vch_descricao,
                        Email = @t.@t.@t.usuario.vch_email,
                        Prioridade = @t.@t.@t.@t.prioridade.vch_descricao,
                        NomeFuncionario = @t.@t.@t.usuario.vch_nome,
                        Status = @t.@t.@t.@t.@t.status.vch_descricao
                    })).FirstOrDefault();

            }
            return _chamadoViewModel;

        }
    }
}

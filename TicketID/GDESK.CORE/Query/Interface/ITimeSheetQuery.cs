﻿using System.Collections.Generic;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.CORE.Query.Interface
{
    public interface ITimeSheetQuery
    {
        ITimeSheetQuery Parameter(TimeSheetFiltro paramter);
        IEnumerable<TimeSheetViewModel> Execute();
    }
}

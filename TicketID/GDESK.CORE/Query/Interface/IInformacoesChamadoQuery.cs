﻿using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query.Interface
{
    public interface IInformacoesChamadoQuery
    {
        IInformacoesChamadoQuery Parameter(int codigoChamado);
        DetalhesChamadoViewModel Execute();
    }
}

﻿using GDESK.CORE.ViewModel.Control;
using System.Collections.Generic;
namespace GDESK.CORE.Query.Interface
{
    public interface ITrocarAnalistaListaQuery
    {
        IList<TrocarAnalistaViewModel> Execute();
    }
}

﻿using System.Collections.Generic;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query.Interface
{
    public interface IListaDeChamadosQuery
    {
        IListaDeChamadosQuery Paramter(FiltroChamdoViewModel parameter = null);
        IEnumerable<GridChamadoViewModel> Execute();
    }
}

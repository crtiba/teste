﻿using System.Collections.Generic;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query.Interface
{
    public interface IInformacoesGraficoQuery
    {
        IInformacoesGraficoQuery Parameter(int codigoChamado);
        IList<GraficoViewModel> Execute();
    }
}

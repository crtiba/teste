﻿using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query.Interface
{
    public interface INovoChamadoFeedQuery
    {
        INovoChamadoFeedQuery Parameter(int codigoSolicitante);
        NovoChamadoFeedViewModel Execute();
    }
}

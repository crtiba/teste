﻿using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query.Interface
{
    public interface IInformacoesEmailQuery
    {
        IInformacoesEmailQuery Paramenter(int codigoChamado);
        EmailChamadoViewModel Execute();
    }
}

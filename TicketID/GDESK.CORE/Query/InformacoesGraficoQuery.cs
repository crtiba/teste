﻿using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using GDESK.CORE.Database;
using GDESK.CORE.Query.Interface;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.ViewModel.Control;

namespace GDESK.CORE.Query
{
    public class InformacoesGraficoQuery : IInformacoesGraficoQuery
    {
        private int _codigoChamado;
        private readonly IHorario _horario;


        public InformacoesGraficoQuery()
        {
             _horario = new Horario();
        }

        public IInformacoesGraficoQuery Parameter(int codigoChamado)
        {
            _codigoChamado = codigoChamado;
           
            return this;
        }

        public IList<GraficoViewModel> Execute()
        {

            var now = _horario.Agora();

            using (var context = new ModelContainer())
            {

                var query = (from historicoChamado in context.HistoricoChamado
                             join statusChamado in context.Status on
                                 historicoChamado.Status_pk_int_status equals statusChamado.pk_int_status
                             where historicoChamado.Chamado_pk_int_chamado == _codigoChamado
                             select new GraficoViewModel
                             {
                                 Descricao = statusChamado.vch_descricao,
                                 Segundos =
                                     SqlFunctions.DateDiff("SECOND", historicoChamado.dt_De, (historicoChamado.dt_Ate ?? now)).Value
                             }).GroupBy(x => x.Descricao);

                var lst = new List<GraficoViewModel>();

                query.ToList().ForEach(item => lst.Add(new GraficoViewModel {Descricao = item.Key,Segundos = item.Sum(x=>x.Segundos)}));



                return lst;
            }
        }
    }
}

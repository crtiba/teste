﻿/// <reference path="jquery-1.6.4.min.js" />
/// <reference path="jquery.signalR-1.1.4.js" />

var feed = $.connection.newFeed;


$(function () {

    feed.client.ReceberMensagem = function (solicitante, avatar, assunto, mensagem, url) {
        var templateFeed = $("#NewsFeed");
        $(templateFeed).find('.panel-heading').text(solicitante + " - " + assunto);
        $(templateFeed).find('.panel-body').click(function () {
            location.href = url;
        });
        $(templateFeed).find("img").attr("src", avatar);
        $(templateFeed).find("#feedMensagem").text(mensagem);

        $(templateFeed).fadeIn('slow').delay(4000).fadeOut('slow');

        if ($.isFunction(ObterLista)) ObterLista();
    };

    feed.client.Notificar = Notificar;
    
    $.connection.hub.start();
});

function EnviarMensagem(solicitante,avatar,assunto,mensagem,url) {
   
    feed.server.enviarMensagem(solicitante,avatar,assunto,mensagem,url);
}


function EnviarNotificao(msg, analista, codigoChamado) {

    feed.server.notificar(msg, analista, parseInt(codigoChamado));

}

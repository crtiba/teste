﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using GDESK.CORE.Enum;
using GDESK.CORE.Service;
using GDESK.CORE.Service.Interface;
using GDESK.CORE.UserControl;
using GDESK.CORE.ViewModel.Page;

namespace GDESK.WEB.Areas.Analista.Controllers
{
    public class TimeSheetController : Controller
    {

        private readonly ITimeSheetService _timeSheetService;
        private readonly IHorario _horario;

        public TimeSheetController()
        {
            _timeSheetService = new TimeSheetService();
            _horario = new Horario();
        }
        // GET: /Analista/TimeSheet/

        [Authorize]
        public ActionResult Index()
        {

            var lst = ConsultarTimeSheet(Suporte.UsuarioLogado.CodigoUsuario, _horario.Agora().Month);


            return View(lst);
        }

        [HttpPost]
        public void ApontarHoras(TimeSheetViewModel model)
        {
            _timeSheetService.ApontarHora(model);

        }
        [HttpPost]
        public ActionResult Index(int codigoAnalista, int codigoMes)
        {
            var lst = ConsultarTimeSheet(codigoAnalista, codigoMes);

            return View(lst);
        }

        private IEnumerable<TimeSheetViewModel> ConsultarTimeSheet(int codigoAnalista, int codigoMes)
        {
            var lst = _timeSheetService.TimeSheetList(codigoAnalista, codigoMes).ToList();
            var ultimoDiaLancamento = lst.Any() ? (lst.Max(x => x.DiaApontamento) + 1) : _horario.Agora().Day;
            ViewBag.DiaDe = ultimoDiaLancamento = ultimoDiaLancamento > 0 ? ultimoDiaLancamento : _horario.Agora().Day;
            ViewBag.DiaAte = ultimoDiaLancamento;

            ViewBag.TotalApontado =
                lst.Where(x => x.TipoHora == TipoHora.Normal)
                .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);

            ViewBag.TotalAprovado =
                lst.Where(x => x.TipoHora == TipoHora.Normal && x.Aprovado == true)
                    .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);


            ViewBag.TotalReprovado =
                lst.Where(x => x.TipoHora == TipoHora.Normal && x.Aprovado == false)
                    .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);

            ViewBag.TotalApontadoExtra =
                lst.Where(x => x.TipoHora == TipoHora.Extra)
                    .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);

            ViewBag.TotalAprovadoExtra =
                lst.Where(x => x.TipoHora == TipoHora.Extra && x.Aprovado == true)
                    .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);

            ViewBag.TotalReprovadoExtra =
                lst.Where(x => x.TipoHora == TipoHora.Extra && x.Aprovado == false)
                    .Sum(x => x.TotalHoras != null ? x.TotalHoras.Value.Hours : 0);

            ViewBag.AnoApontamento = _horario.Agora().Year;
            ViewBag.CodigoMes = _horario.Agora().Month;
            ViewBag.Mes = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ViewBag.CodigoMes);
            ViewBag.Recursos = _timeSheetService.RecursosAguarandoAprovacao();

            return lst;
        }

        [HttpPost]
        public ActionResult AprovarApontamentos(ApontamentoViewModel[] apontamentos)
        {
            _timeSheetService.AlterarStatus(apontamentos);

            return null;
        }


    }
}
